from Retorno.Mensajes import Mensajes
import webbrowser
class ReporteErroresHtml():
    def __init__(self,listaMensajes):
        self.listaMensajes = listaMensajes
        self.crearHtml()

    def crearHtml(self):
        hayErroresPorImprimir = False
        if len(self.listaMensajes)!=0:
            texto = '''
            <html>
            <head>
                <title>Tabla de Errores, proyecto 2</title>
            </head>
            <table BORDER="4">
                <tr>
                    <th>cont</th>
                    <th>TipoError</th>
                    <th>Instruccion</th>
                    <th>Error</th>
                    <th>Descripcion</th>
                    <th>fila</th>
                    <th>columna</th>
                </tr>
            '''
            cont = 0
            for m in self.listaMensajes:
                if m.tipo != 5:# tipo 5 es del tipo imprimir
                    hayErroresPorImprimir = True
                    msj : Mensajes = m
                    texto = texto + "\n" + '''
                    <tr>
                        <td>'''+str(cont)+'''</td>
                        <td>'''+str(msj.getTipoError())+'''</td>
                        <td>'''+str(msj.nombreInstruccion)+'''</td>
                        <td>'''+str(msj.error)+'''</td>
                        <td>'''+str(msj.descripcion)+'''</td>
                        <td>'''+str(msj.fila)+'''</td>
                        <td>'''+str(msj.columna)+'''</td>
                    </tr>
                    '''
                    cont = cont + 1
            texto = texto + '''
            </table>
            </html>
            '''
        if hayErroresPorImprimir:
            self.guardarArchivo(texto)

    
    def guardarArchivo(self,data):
        f = open('Reportes/Errores.html','w')
        f.write(data)
        f.close()
        webbrowser.open_new_tab('Reportes/Errores.html')
