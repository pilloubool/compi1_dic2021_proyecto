from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno
from Entorno.Simbolo import Simbolo
from Entorno.Tipo import Tipo

class Identificador(Instruccion):
    def __init__(self, fila, columna, identificador):
        self.identificador = identificador
        self.fila = fila
        self.columna = columna
        self.tipo = Tipo.NULLO

    def ejecutar(self, entorno: Entorno,listaMensajes):
        simbolo: Simbolo = entorno.buscarSimbolo(self.identificador)
    
        if simbolo != None:
            self.tipo = simbolo.tipo
            return simbolo.valor
        return None

    def getTipo(self):
        return self.tipo

