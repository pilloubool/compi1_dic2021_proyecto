from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno
from Instrucciones.Break import Break
from Instrucciones.Return import Return



class ELSEIF(Instruccion):
    def __init__(self, fila, columna,condicion,instrucciones):
        self.fila  = fila
        self.columna = columna
        self.condicion = condicion
        self.instrucciones = instrucciones
        self.bander = False

    def ejecutar(self, entPadre:Entorno, listaMensajes):
        entElseIf = Entorno(entPadre)
        
        valor = self.condicion.ejecutar(entElseIf,listaMensajes)
        if valor:
            entIns = Entorno(entPadre)
            for i in self.instrucciones:
                retorno =   i.ejecutar(entIns,listaMensajes)
                if isinstance(retorno,Break):
                    return retorno
                elif isinstance(retorno,Return):
                    return retorno
            return True
        return False