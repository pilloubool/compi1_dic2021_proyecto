from Interface.Instruccion import Instruccion
from Entorno.Tipo import Tipo
from Retorno.Mensajes import Mensajes
# una dimencion corresponde a UN VECTOR
class DimensionInstancia(Instruccion):
    def __init__(self, fila, columna,LexpDim,nombre,cantDim):
        self.fila = fila
        self.columna = columna
        self.LexpDim = LexpDim
        self.nombre = nombre
        self.cantDim = cantDim
        self.arregloPadre = []

    def ejecutar(self, entorno, listaMensajes):
        return self.recursivo(entorno,listaMensajes)

    def recursivo(self,entorno,listaMensajes):
        cont = 0
        NuevaLista = []

        if self.LexpDim == None and self.cantDim is not None:
                print()
                tamanoDim = 5
        
                while(cont != tamanoDim):
                    cont2 = 0
                    listaAux = []
                    while(cont2 != tamanoDim):
                        cont3 = 0
                        listaAux3 = []
                        while(cont3 !=tamanoDim):
                            cont4 = 0
                            listaAux4 = []
                            while(cont4 !=tamanoDim):
                                cont5 = 0
                                listaAux5 = []
                                while(cont5 !=tamanoDim):
                                    listaAux5.append(Tipo.NULLO)
                                    cont5 = cont5 + 1
                                listaAux4.append(listaAux5)
                                cont4 = cont4 + 1
                            listaAux3.append(listaAux4)
                            cont3 = cont3 + 1
                        listaAux.append(listaAux3)
                        cont2 = cont2 + 1
                    NuevaLista.append(listaAux)
                    cont = cont + 1
                return NuevaLista

        elif self.LexpDim is not None and self.cantDim == None:
            t = len(self.LexpDim)
            if t == 1:
                tamanoDim = self.LexpDim[0].ejecutar(entorno,listaMensajes)
                while(cont != tamanoDim):
                    NuevaLista.append(Tipo.NULLO)
                    cont = cont + 1
            elif t == 2:
                tamanoDim = self.LexpDim[0].ejecutar(entorno,listaMensajes)
                tamanoDim2 = self.LexpDim[1].ejecutar(entorno,listaMensajes)
                while(cont != tamanoDim):
                    cont2 = 0
                    listaAux = []
                    while(cont2 != tamanoDim2):
                        listaAux.append(Tipo.NULLO)
                        cont2 = cont2 + 1
                    NuevaLista.append(listaAux)
                    cont = cont + 1
            elif t == 3 :
                tamanoDim = self.LexpDim[0].ejecutar(entorno,listaMensajes)
                tamanoDim2 = self.LexpDim[1].ejecutar(entorno,listaMensajes)  
                tamanoDim3 = self.LexpDim[2].ejecutar(entorno,listaMensajes)      
                while(cont != tamanoDim):
                    cont2 = 0
                    listaAux = []
                    while(cont2 != tamanoDim2):
                        cont3 = 0
                        listaAux3 = []
                        while(cont3 !=tamanoDim3):
                            listaAux3.append(Tipo.NULLO)
                            cont3 = cont3 + 1
                        listaAux.append(listaAux3)
                        cont2 = cont2 + 1
                    NuevaLista.append(listaAux)
                    cont = cont + 1
            elif t == 4 :
                tamanoDim = self.LexpDim[0].ejecutar(entorno,listaMensajes)
                tamanoDim2 = self.LexpDim[1].ejecutar(entorno,listaMensajes)  
                tamanoDim3 = self.LexpDim[2].ejecutar(entorno,listaMensajes)   
                tamanoDim4 = self.LexpDim[3].ejecutar(entorno,listaMensajes)      

                while(cont != tamanoDim):
                    cont2 = 0
                    listaAux = []
                    while(cont2 != tamanoDim2):
                        cont3 = 0
                        listaAux3 = []
                        while(cont3 !=tamanoDim3):
                            cont4 = 0
                            listaAux4 = []
                            while(cont4 !=tamanoDim4):
                                listaAux4.append(Tipo.NULLO)
                                cont4 = cont4 + 1
                            listaAux3.append(listaAux4)
                            cont3 = cont3 + 1
                        listaAux.append(listaAux3)
                        cont2 = cont2 + 1
                    NuevaLista.append(listaAux)
                    cont = cont + 1

            elif t == 5 :
                tamanoDim = self.LexpDim[0].ejecutar(entorno,listaMensajes)
                tamanoDim2 = self.LexpDim[1].ejecutar(entorno,listaMensajes)  
                tamanoDim3 = self.LexpDim[2].ejecutar(entorno,listaMensajes)   
                tamanoDim4 = self.LexpDim[3].ejecutar(entorno,listaMensajes)     
                tamanoDim5 = self.LexpDim[4].ejecutar(entorno,listaMensajes)      


                while(cont != tamanoDim):
                    cont2 = 0
                    listaAux = []
                    while(cont2 != tamanoDim2):
                        cont3 = 0
                        listaAux3 = []
                        while(cont3 !=tamanoDim3):
                            cont4 = 0
                            listaAux4 = []
                            while(cont4 !=tamanoDim4):
                                cont5 = 0
                                listaAux5 = []
                                while(cont5 !=tamanoDim5):
                                    listaAux5.append(Tipo.NULLO)
                                    cont5 = cont5 + 1
                                listaAux4.append(listaAux5)
                                cont4 = cont4 + 1
                            listaAux3.append(listaAux4)
                            cont3 = cont3 + 1
                        listaAux.append(listaAux3)
                        cont2 = cont2 + 1
                    NuevaLista.append(listaAux)
                    cont = cont + 1
            else : 
                listaMensajes.append(Mensajes(3, "ArregloInstancia T1", "dimensiones", "ArrT1("+str(self.nombre)+"), solo se admite un maximo de 5 dimenciones, se intenta crear uno de: \""+str(t)+"\" dimensiones", self.fila, self.columna))
                return None 
            return NuevaLista