from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno
from Instrucciones.Break import Break
from Instrucciones.Return import Return


class FOR(Instruccion):
    def __init__(self, fila, columna,ExpAsigna,ExpValida,ExpIncremento ,instrucciones):
        self.fila = fila
        self.columna = columna
        self.ExpAsigna = ExpAsigna
        self.ExpValida = ExpValida
        self.ExpIncremento = ExpIncremento
        self.instrucciones = instrucciones

    def ejecutar(self, entPadre:Entorno, listaMensajes):
        entFor = Entorno(entPadre)
        #   asignamos o declaramos la variable en el ciclo for
        self.ExpAsigna.ejecutar(entFor,listaMensajes)
        while(True):
            #revisamos si la expValidacion si es true o false
            v = self.ExpValida.ejecutar(entFor,listaMensajes)
            if isinstance(v, bool):
                if v:
                    entParaUnCiclo = Entorno(entFor) 
                    for ins in self.instrucciones:
                        retorno = ins.ejecutar(entParaUnCiclo,listaMensajes)
                        if isinstance(retorno, Break):
                            return self # al aparecer break, retornaremos el propio for, para salir unicamente de este ciclo y no de otros
                        elif isinstance(retorno, Return):
                            return Return
                else    :   break
                #incrementamos, para el siguiente siclo
                self.ExpIncremento.ejecutar(entFor,listaMensajes)




        









