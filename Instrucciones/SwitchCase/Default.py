from Interface.Instruccion import Instruccion
from Instrucciones.Break import Break

class Default(Instruccion):
    def __init__(self, fila, columna,Linstruciones):
        self.fila = fila
        self.columna = columna
        self.Linstruciones = Linstruciones

    def ejecutar(self, entorno, listaMensajes):
        for ins in self.Linstruciones:
            retorno =   ins.ejecutar(entorno,listaMensajes)
            if isinstance(retorno, Break):
                return retorno