from Interface.Instruccion import Instruccion
from Entorno.Tipo import Tipo

class Primitivo(Instruccion):
    def __init__(self, fila, columna, tipo:Tipo, valor):
        self.tipo = tipo
        self.valor = valor
        self.fila = fila
        self.columna = columna

    def ejecutar(self, entorno,listaMensajes):
        return self.valor

    def getTipo(self):
        return self.tipo