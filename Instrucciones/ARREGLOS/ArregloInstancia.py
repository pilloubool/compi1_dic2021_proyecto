from Interface.Instruccion import Instruccion
from Instrucciones.ARREGLOS.DimensionInstancia import DimensionInstancia
from Instrucciones.ARREGLOS.datoArreglo import datoArreglo

from Retorno.Mensajes import Mensajes
from Entorno.Simbolo import Simbolo
from Entorno.Entorno import Entorno


class ArregloInstancia(Instruccion):
    def __init__(self, fila, columna,tipo,cantDim,nombreArreglo,tipoAsignacion,LexpDim):
        self.fila = fila
        self.columna = columna
        self.tipo = tipo  # tipo izq, con el que se declara el arreglo
        self.cantDim = cantDim  #conteo de dimensiones del arreglo
        self.nombreArreglo = str(nombreArreglo).lower() 
        self.tipoAsignacion = tipoAsignacion  #indica el tipo de la nueva instancia, debe conicidir con el tipo
        self.LexpDim = LexpDim  # lista de expreciones, que corresponden al tamaño de cada una de las n posibles dimensiones 
        self.listaDedimensiones = []

    '''
    Explicacion de manejo de dimensiones, en la declracion del vector tipo 1 (por instancia):
        +.  se trabajara las n dimensiones, como una lista de listas 
        +.  la lista inicial que contendra cada una de las dimensiones es: " self.listaDedimensiones "
        +.  por cada dimencion nueva, se creara un vector con dimencion X, el tamaño esta descrito por la var: "self.cantDim"
    '''

    def ejecutar(self, entorno:Entorno, listaMensajes):
        #validacion de tipos 
        if self.tipoAsignacion==None and self.LexpDim==None:#si es una declaracion tipo, parametro de la funcion
            print()
            dim = DimensionInstancia(self.fila, self.columna, None, self.nombreArreglo,self.cantDim)
            retorno = dim.ejecutar(entorno, listaMensajes)
            self.listaDedimensiones = retorno
            return self

        elif self.tipo == self.tipoAsignacion:#si es una delcara, tipo instruccion
            if self.LexpDim is not None:
                if len(self.LexpDim) == self.cantDim:

                    dim = DimensionInstancia(self.fila, self.columna, self.LexpDim,self.nombreArreglo,None)
                    retorno = dim.ejecutar(entorno, listaMensajes)
                    if retorno == None:
                        return None 
                    self.listaDedimensiones = retorno
                    #2. Guardamos el arreglo en el entorno actual
                    #2.1  creamos el simbolo a guardar
                    sim = Simbolo(self.tipo,self.nombreArreglo,self,self.fila)
                    #2.2   buscamos si existe algun simbolo con este nombre en el entorno
                    if entorno.buscarSimbolo(sim.nombre) == None:
                        #2.3 guardamos el simbolo en el entorno
                        entorno.nuevoSimbolo(sim)
                    else:
                        listaMensajes.append(Mensajes(3, "Arreglo T1", "Declaracion", "Ya existe un ArregloT1 con este nombre: \""+self.nombreArreglo+"\"", self.fila, self.columna))
                else:
                    listaMensajes.append(Mensajes(3, "Arreglo T1", "dimensiones no concuerdan", "la definicion de dimensiones en declaracion no es igual a la dimension de asignacion", self.fila, self.columna))
            else:
                listaMensajes.append(Mensajes(3, "Arreglo T1", "Asignacion == NULL", "No existe tamano para el arreglo: \""+self.nombreArreglo+"\"", self.fila, self.columna))
        else:
            listaMensajes.append(Mensajes(3, "Arreglo T1", "De tipos", "En ArregloT1("+self.nombreArreglo+"), tipo declaracion ("+str(self.tipo)+") , no es igual al tipo Asignacion ("+ str(self.tipoAsignacion)+")", self.fila, self.columna))
            


    def getTipo(self):
        return self.tipo

    def modificacionArreglo(self,cont,lcoordenadas,arreglo,listaMensajes,toKeep):
        self.modificacionArreglo2(cont, lcoordenadas, self.listaDedimensiones, listaMensajes, toKeep)


    def modificacionArreglo2(self,cont,lcoordenadas,arreglo,listaMensajes,toKeep):
        if isinstance(arreglo, list):
            pos = lcoordenadas[cont] #por que las coordenadas, las manejares desde 0 (cero), y la expresion nos retorna desde 1
            #comprobamos que el arreglo cuente con un tamaño que este dentro de la coordenada
            if pos < len(arreglo):
                if isinstance(arreglo,list):
                    if not isinstance(arreglo[pos], list):
                        arreglo[pos] = toKeep
                        #arreglo[pos] = datoArreglo(self.tipo, toKeep)
                        print("")
                    else:
                        return self.modificacionArreglo2(cont + 1 , lcoordenadas, arreglo[pos],listaMensajes,toKeep)  
            else : 
                #retorno de Error, por que se quiere acceder, a una posicion del arreglo, que no existe 
                listaMensajes.append(Mensajes(3, "Modificacion_Arreglo", "Dimensiones", "se busca una dimension que esta fuera de los limites del arreglo: \""+str(self.nombre)+"\" ", self.fila, self.columna))            
                return None
        else : 
            #arreglo = toKeep
            print("")
            #modificamos la posicion del arreglo
            #return arreglo # retornara el ultimo dato del arreglo (dato tipo primitivo,data buscado)
 