from enum import Enum

class Tipo(Enum):
    ENTERO = 1
    DOBLE = 2
    BOOLEANO = 3
    CHAR = 4
    CADENA = 5
    NULLO = 6
    VOID = 7