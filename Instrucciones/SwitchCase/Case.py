from Interface.Instruccion import Instruccion
from Instrucciones.Break import Break

class Case(Instruccion):
    def __init__(self, fila, columna,expCase,Linstruciones):
        self.fila = fila
        self.columna = columna
        self.expCase = expCase
        self.Linstruciones = Linstruciones
        self.seEjecutoEsteCase = False

    '''
    aparecioBreak_enElCaseAnterior
        0   =   NO se encontro BREAK y NO se a ejecutado ningun CASE, estado inicial
        1   =   si aparecio BREAK, pero no entrara nunca con uno, por que si aparece break, simplemente deja de ejecutar los cases
        2   =   no aparecio BREAK, pero un case ya fue ejecutado, entonces seguimos con el siguiente case
    '''    

    def ejecutar(self, entorno, listaMensajes,valorIdSwitch,aparecioBreak_enElCaseAnterior):
        valorExpCase = self.expCase.ejecutar(entorno,listaMensajes)
        if valorIdSwitch == valorExpCase or aparecioBreak_enElCaseAnterior == 2:
            self.seEjecutoEsteCase = True
            for ins in self.Linstruciones:
                retorno = ins.ejecutar(entorno,listaMensajes)
                if isinstance(retorno, Break):
                    return retorno
            return self                
        else : return False
