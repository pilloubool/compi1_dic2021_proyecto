from Interface.Instruccion import Instruccion
from Retorno.Mensajes import Mensajes


class Imprimir(Instruccion):

    def __init__(self, fila, columna, expresion):
        self.fila = fila
        self.columna = columna
        self.expresion = expresion

    def ejecutar(self, entorno,listaMensajes):
        valor = self.expresion.ejecutar(entorno,listaMensajes)
        listaMensajes.append(Mensajes(5, "", "", str(valor), self.fila  , self.columna))
        #if valor != None : listaMensajes.append(Mensajes(5, "", "", str(valor), self.fila  , self.columna))
        #else: listaMensajes.append(Mensajes(3, "Imprimir", "Null", "Se intenta imprimir un valor NULL", self.fila, self.columna))

  