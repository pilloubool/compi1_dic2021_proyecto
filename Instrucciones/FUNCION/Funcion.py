from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno
from Entorno.Simbolo import Simbolo
from Entorno.Tipo import Tipo
from Retorno.Mensajes import Mensajes
from Instrucciones.Return import Return

class Funcion(Instruccion):
    def __init__(self, fila, columna,tipo,nombre,Lparametros,Linstr):
        self.fila = fila
        self.columna = columna
        self.tipo = tipo
        self.nombre = nombre
        self.Lparametros = Lparametros
        self.Linstr = Linstr

    def ejecutar(self, ent:Entorno, listaMensajes):
        existeReturn = False
        for inst in self.Linstr:
            if isinstance(inst, Return):
                existeReturn = True

        #validando los posibles errores en la declaracion de la funcion
        if self.tipo == Tipo.VOID and existeReturn:
            listaMensajes.append(Mensajes(3, "Funcion", "void con Return", "una funcion tipo Void no puede traer un Return", self.fila, self.columna))
        elif self.tipo !=Tipo.VOID and self.tipo != Tipo.NULLO  and  not existeReturn: #funcion tipo primitivo debe traer un Return
            listaMensajes.append(Mensajes(3, "Funcion", "return", "la funcion: \""+str(self.nombre)+"("+str(self.tipo)+")\", no cuenta con un Return ", self.fila, self.columna))
        elif self.tipo == Tipo.NULLO:
            listaMensajes.append(Mensajes(3, "Funcion", "funcion=NULL", "la funcion no puede ser declara de tipo NULL", self.fila, self.columna))
        else : # si todo esta correcto, Guardamos la funcion
            sim:Simbolo = Simbolo(self.tipo,self.nombre,self,self.fila)
            if ent.buscarSimbolo(sim.nombre) == None:
                ent.nuevoSimbolo(sim)
            else:
                listaMensajes.append(Mensajes(3, "Funcion", "Funcion ya existe", "Ya fue declarada una Funcion con este nombre: \""+str(sim.nombre)+"\"", self.fila, self.columna))