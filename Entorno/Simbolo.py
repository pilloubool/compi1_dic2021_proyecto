from Entorno.Tipo import Tipo

class Simbolo:
    def __init__(self, tipo = Tipo.NULLO , nombre="", valor=None, linea=0):
        self.tipo = tipo
        self.nombre = nombre
        self.valor = valor
        self.linea = linea

    def getTipo(self):
        return self.tipo
