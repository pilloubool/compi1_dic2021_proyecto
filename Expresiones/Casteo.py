from Interface.Instruccion import Instruccion
from Entorno.Tipo import Tipo
from Retorno.Mensajes import Mensajes


class Casteo(Instruccion):
    def __init__(self, fila, columna,tipo:Tipo,exp):
        self.fila = fila
        self.columna = columna
        self.tipo = tipo    #tipo a castear
        self.exp = exp

    def ejecutar(self, entorno, listaMensajes):
        expResuelta = self.exp.ejecutar(entorno,listaMensajes)
        if expResuelta != None:
            #comprobamos tipos
            #origen
            if self.tipo == Tipo.ENTERO     :   return self.castTo_Int(expResuelta,listaMensajes)
            elif self.tipo == Tipo.DOBLE    :   return self.castTo_double(expResuelta,listaMensajes)
            elif self.tipo == Tipo.BOOLEANO :   return self.castTo_boolean(expResuelta,listaMensajes)
            elif self.tipo == Tipo.CHAR     :   return self.castTo_char(expResuelta,listaMensajes)
            elif self.tipo == Tipo.CADENA   :   return self.castTo_String(expResuelta,listaMensajes)
            else:
                listaMensajes.append(Mensajes(3, "Casteo", "De tipos", "no es posible castear un,   exp:\"" +str(self.exp.tipo) +"\" a ["+ str(self.tipo)+"]", self.fila, self.columna))
                return Tipo.NULLO
        else:
            listaMensajes.append(Mensajes(3, "Casteo", "return = null", "al ejecutar la EXP retorna NULL", self.fila, self.columna))
            return Tipo.NULLO



        #destinos
    def castTo_Int(self,expResuelta,listaMensajes):
        self.tipo = Tipo.ENTERO
        if self.exp.tipo == Tipo.DOBLE:
            return int(expResuelta)
        elif self.exp.tipo == Tipo.CHAR:
            return ord(expResuelta)
        elif self.exp.tipo == Tipo.CADENA:
            if expResuelta.isnumeric():
                return int(expResuelta)
            else : 
                listaMensajes.append(Mensajes(3, "Casteo", "De tipos a", "se quiere castear: exp(\""+str(expResuelta)+"\" ,Tipo.Cadena) a [Tipo.ENTERO], Pero EXP, no es numerico",self.fila , self.columna))
                self.tipo = Tipo.NULLO
                return None
        else : 
            listaMensajes.append(Mensajes(3, "Casteo", "De Tipos a", "No se permite castear de:  exp(\""+str(expResuelta)+"\" ,  "+str(self.exp.tipo)+") a ["+str(self.tipo)+"]", self.fila, self.columna))
            return Tipo.NULLO

    def castTo_double(self,expResuelta,listaMensajes):
        self.tipo = Tipo.DOBLE
        if self.exp.tipo == Tipo.ENTERO:
            return float(expResuelta)
        elif self.exp.tipo == Tipo.CADENA:
            if expResuelta.isnumeric():
                return float(expResuelta)
            else : 
                listaMensajes.append(Mensajes(3, "Casteo", "De tipos b", "se quiere castear: exp(\""+str(expResuelta)+"\" ,Tipo.Cadena) a [Tipo.DOUBLE], Pero EXP, no es numerico",self.fila , self.columna))
                self.tipo = Tipo.NULLO
                return None
        elif self.exp.tipo == Tipo.CHAR:
            return float(ord(expResuelta))    
        else:
            listaMensajes.append(Mensajes(3, "Casteo", "De Tipos b", "No se permite castear de:  exp(\""+str(expResuelta)+"\" ,  "+str(self.exp.tipo)+") a ["+str(self.tipo)+"]", self.fila, self.columna))
            self.tipo = Tipo.NULLO
            return None

    def castTo_boolean(self,expResuelta,listaMensajes):
        self.tipo = Tipo.NULLO
        listaMensajes.append(Mensajes(3, "Casteo", "De tipos c", "No se permite casteos con Booleanos", self.fila, self.columna))
        return None

    def castTo_char(self,expResuelta,listaMensajes):
        self.tipo = Tipo.CHAR
        if self.exp.tipo == Tipo.ENTERO  or self.exp.tipo == Tipo.DOBLE:
            return chr(int(expResuelta))
        listaMensajes.append(Mensajes(3, "Casteo", "De Tipos d", "No se permite castear de:  exp(\""+str(expResuelta)+"\" ,  "+str(self.exp.tipo)+") a ["+str(self.tipo)+"]", self.fila, self.columna))
        return None

    def castTo_String(self,expResuelta,listaMensajes):
        self.tipo = Tipo.NULLO
        if self.exp.tipo == Tipo.ENTERO  or  self.exp.tipo == Tipo.DOBLE  or  self.exp.tipo == Tipo.BOOLEANO:
            self.tipo = Tipo.CADENA
            return str(expResuelta)
        
        listaMensajes.append(Mensajes(3, "Casteo", "De Tipos e", "No se permite castear de:  exp(\""+str(expResuelta)+"\" ,  "+str(self.exp.tipo)+") a ["+str(self.tipo)+"]", self.fila, self.columna))
        return None

    def getTipo(self):
        return self.tipo











