from Interface.Instruccion import Instruccion
from Entorno.Tipo import Tipo
from Retorno.Mensajes import Mensajes
from Instrucciones.ARREGLOS.ArregloInstancia import ArregloInstancia
from Instrucciones.ARREGLOS.ArregloLista import ArregloLista

import math

class FuncionesNativas(Instruccion):
    def __init__(self, fila, columna,nombreNativa,exp):
        self.fila = fila
        self.columna = columna
        self.nombreNativa = nombreNativa
        self.exp = exp #la exp debe ser tipo cadena, de lo contrario error
        self.tipo = Tipo.NULLO

    def ejecutar(self, entorno, listaMensajes):
        retorno = self.exp.ejecutar(entorno,listaMensajes)
        t_exp = self.exp.tipo
        t = Tipo.NULLO
        if retorno != None:
            if self.nombreNativa == "aminus":
                t = Tipo.CADENA
                if t_exp == Tipo.CADENA:
                    self.tipo = Tipo.CADENA
                    return str(retorno).lower()

            elif self.nombreNativa == "amayus":
                t = Tipo.CADENA
                if t_exp == Tipo.CADENA:
                    self.tipo = Tipo.CADENA
                    return str(retorno).upper()

            elif self.nombreNativa == "truncate":
                t = Tipo.DOBLE
                if t_exp == Tipo.DOBLE:
                    self.tipo = Tipo.ENTERO
                    return int(retorno)
            
            elif self.nombreNativa == "round":
                t = Tipo.DOBLE
                if t_exp == Tipo.DOBLE:
                    self.tipo = Tipo.DOBLE
                    decimal,entero = math.modf(float(retorno))
                    if decimal >= 0.5   :   entero = entero + 1
                    else    :   entero = entero - 1
                    return float(entero)
            elif self.nombreNativa == "tamano":
                arreglo = retorno.listaDedimensiones
                if isinstance(arreglo, list):
                    self.tipo = Tipo.ENTERO
                    tamanoArreglo = len(arreglo)
                    return tamanoArreglo
                else:
                    listaMensajes.append(Mensajes(3, str(self.nombreNativa).upper()+"()", "de Tipos", "la nativa Tamano(), solo admite ARREGLOS como parametros, se envio: \" "+str(retorno)+",("+t_exp+") \"", self.fila, self.columna))    

            listaMensajes.append(Mensajes(3, str(self.nombreNativa).upper()+"()", "de Tipos", "la funcion "+str(self.nombreNativa).upper()+"(), unicamente adminte \""+str(t)+"\",  pero es:("+str(self.exp.tipo)+")", self.fila, self.columna))
        else:
            listaMensajes.append(Mensajes(3, str(self.nombreNativa).upper()+"()", "de Tipos", "la funcion "+str(self.nombreNativa).upper()+"(), No permite parametros = NULL", self.fila, self.columna))

        self.tipo = Tipo.NULLO
        return Tipo.NULLO






    def getTipo(self):
        return self.tipo