from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno
from Entorno.Tipo import Tipo

class Return(Instruccion):
    def __init__(self, fila, columna,exp):
        self.fila = fila
        self.columna = columna
        self.exp = exp
        self.tipo = Tipo.NULLO

    def ejecutar(self, entorno, listaMensajes):
        valor = self.exp.ejecutar(entorno,listaMensajes)
        self.tipo = self.exp.tipo
        return self
    

    def getTipo(self):
        return self.tipo