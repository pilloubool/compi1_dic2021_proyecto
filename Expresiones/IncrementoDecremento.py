from Interface.Instruccion import Instruccion
from Entorno.Tipo import Tipo
from Entorno.Entorno import Entorno
from Entorno.Simbolo import Simbolo
from Retorno.Mensajes import Mensajes

class IncrementoDecremento(Instruccion):
    def __init__(self, fila, columna,identificador,operador):
        self.fila = fila
        self.columna = columna
        self.identificador = identificador
        self.operaciones = operador
        self.tipo = Tipo.NULLO

    def ejecutar(self, ent:Entorno, listaMensajes):
        sim : Simbolo = ent.buscarSimbolo(self.identificador)

        if sim != None:
            if sim.tipo == Tipo.ENTERO or sim.tipo==Tipo.DOBLE:
                self.tipo = sim.tipo
                if self.operaciones == "++" : sim.valor = sim.valor + 1
                else    :   sim.valor = sim.valor - 1
                return sim.valor
            else:
                listaMensajes.append(Mensajes(3, "(++,--)", "De tipos", "solo se permite en INC/DECR var INT/DOUBLE  var:{"+str(self.identificador)+"  tipo: ("+ str(sim.tipo)+")}", self.fila, self.columna))
                return None
        listaMensajes.append(Mensajes(3, "++,--", "No exite simbolo", "la variable no existe: "+str(self.identificador), self.fila, self.columna))
        return None

    def getTipo(self):
        return self.tipo