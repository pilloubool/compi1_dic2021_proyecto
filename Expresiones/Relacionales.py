from Interface.Instruccion import Instruccion
from Entorno.Tipo import Tipo
from Retorno.Mensajes import Mensajes

class Relacionales(Instruccion):
    def __init__(self, fila, columna,operador,hijoIzq,hijoDer):
        self.fila = fila
        self.columna = columna
        self.operador = operador
        self.hijoIzq = hijoIzq
        self.hijoDer = hijoDer
        self.tipo = Tipo.NULLO

    def ejecutar(self, entorno, listaMensajes):
        izq = self.hijoIzq.ejecutar(entorno,listaMensajes)
        der = self.hijoDer.ejecutar(entorno,listaMensajes)

        if self.operador == '==':
            if self.hijoIzq.tipo == Tipo.ENTERO     :   return self.igualacion_int(listaMensajes,izq,der)
            elif self.hijoIzq.tipo == Tipo.DOBLE    :   return self.igualacion_double(listaMensajes,izq,der)
            elif self.hijoIzq.tipo == Tipo.BOOLEANO :   return self.igualacion_boolean(listaMensajes,izq,der)
            elif self.hijoIzq.tipo == Tipo.CHAR     :   return self.igualacion_CHAR(listaMensajes,izq,der)
            elif self.hijoIzq.tipo == Tipo.CADENA   :   return self.igualacion_str(listaMensajes,izq,der)

        elif self.operador == '!=':
            if self.hijoIzq.tipo == Tipo.ENTERO     :   return self.diferenciacion_int(listaMensajes,izq,der)
            elif self.hijoIzq.tipo == Tipo.DOBLE    :   return self.diferenciacion_doble(listaMensajes,izq,der)
            elif self.hijoIzq.tipo == Tipo.BOOLEANO :   return self.diferenciacion_boolean(listaMensajes,izq,der)
            elif self.hijoIzq.tipo == Tipo.CHAR     :   return self.diferenciacion_char(listaMensajes,izq,der)
            elif self.hijoIzq.tipo == Tipo.CADENA   :   return self.diferenciacion_str(listaMensajes,izq,der)

        elif self.operador == '<':
            if self.hijoIzq.tipo == Tipo.ENTERO  or self.hijoIzq.tipo == Tipo.DOBLE :  return self.menorQ_intDoble(listaMensajes,izq,der)
            elif self.hijoIzq.tipo == Tipo.BOOLEANO     :   return self.menorQ_booleano(listaMensajes,izq,der)

        elif self.operador == '>':
            if self.hijoIzq.tipo == Tipo.ENTERO  or self.hijoIzq.tipo == Tipo.DOBLE :  return self.mayorQ_intDoble(listaMensajes,izq,der)
            elif self.hijoIzq.tipo == Tipo.BOOLEANO     :   return self.mayorQ_booleano(listaMensajes,izq,der)

        elif self.operador == '<=':
            if self.hijoIzq.tipo == Tipo.ENTERO  or self.hijoIzq.tipo == Tipo.DOBLE :  return self.menorIgual_intDoble(listaMensajes,izq,der)
            elif self.hijoIzq.tipo == Tipo.BOOLEANO     :   return self.menorIgual_booleano(listaMensajes,izq,der)

        elif self.operador == '>=':
            if self.hijoIzq.tipo == Tipo.ENTERO  or self.hijoIzq.tipo == Tipo.DOBLE :  return self.mayorIgual_intDoble(listaMensajes,izq,der)
            elif self.hijoIzq.tipo == Tipo.BOOLEANO     :   return self.mayorIgual_booleano(listaMensajes,izq,der)

        listaMensajes.append(Mensajes(6, "Relacionales", "no es Un relacional", "no es un simbolo relacional valido", self.fila, self.columna))
        return None

    # ..............................................................................
    def getTipo(self):
        return self.tipo

    # ***********************************************IGUALACION ( == ).**********************************************************************************
    def igualacion_int(self,listaMensajes,izq,der):
        self.tipo = Tipo.BOOLEANO
        castIzq = int(izq)
        if self.hijoDer.tipo == Tipo.ENTERO:
            if castIzq == int(der) :   return True
            return False
        elif self.hijoDer.tipo == Tipo.DOBLE:
            if castIzq == float(der)  :   return True
            return False
        elif self.hijoDer.tipo == Tipo.CADENA:
            if str(castIzq) == str(der) :   return True
            return False
        
        self.tipo = Tipo.NULLO
        listaMensajes.append(Mensajes(3, "==", "Error TiposA", "no se permite comparar(==): \""+str(self.hijoIzq.tipo)+"\" con \"" +str(self.hijoDer.tipo)+"\" , solo int/double/string ", self.fila, self.columna))
        return None
    # ..............................................................................
    def igualacion_double(self,listaMensajes,izq,der):
        self.tipo = Tipo.BOOLEANO
        if self.hijoDer.tipo == Tipo.ENTERO or self.hijoDer.tipo == Tipo.DOBLE:
            if float(izq) == float(der) :   return True
            return False
        elif self.hijoDer.tipo == Tipo.CADENA:
            if str(izq) == str(der) :   return True
            return False

        self.tipo = Tipo.NULLO
        listaMensajes.append(Mensajes(3, "==", "Error TiposB", "no se permite comparar(==): \""+str(self.hijoIzq.tipo)+"\" con \"" +str(self.hijoDer.tipo)+"\" , solo int/double/string ", self.fila, self.columna))
        return None
    # ..............................................................................
    def igualacion_boolean(self,listaMensajes,izq,der):
        self.tipo = Tipo.BOOLEANO
        if self.hijoDer.tipo == Tipo.BOOLEANO or self.hijoDer.tipo == Tipo.CADENA:
            if str(izq).lower() == str(der).lower() : return True
            return False

        self.tipo = Tipo.NULLO
        listaMensajes.append(Mensajes(3, "==", "Error TiposC", "no se permite comparar(==): \""+str(self.hijoIzq.tipo)+"\" con \"" +str(self.hijoDer.tipo)+"\" , solo BOOLEAN/STRING ", self.fila, self.columna))
        return None
    # ..............................................................................
    def igualacion_CHAR(self,listaMensajes,izq,der):
        self.tipo = Tipo.BOOLEANO
        if self.hijoDer.tipo == Tipo.CHAR:
            if str(izq).lower() == str(der).lower() : return True
            return False

        self.tipo = Tipo.NULLO
        listaMensajes.append(Mensajes(3, "==", "Error TiposD", "no se permite comparar(==): \""+str(self.hijoIzq.tipo)+"\" con \"" +str(self.hijoDer.tipo)+"\" , solo CHAR ", self.fila, self.columna))
        return None
    # ..............................................................................
    def igualacion_str(self,listaMensajes,izq,der):
        self.tipo = Tipo.BOOLEANO

        if self.hijoDer.tipo == Tipo.ENTERO :
            if str(izq) == str(int(der)) : return True
            return False
        elif self.hijoDer.tipo == Tipo.DOBLE:
            if str(izq) == str(float(der)) : return True
            return False
        elif self.hijoDer.tipo == Tipo.BOOLEANO:
            if str(izq).lower() == str(bool(der)).lower() : return True
            return False
        elif self.hijoDer.tipo == Tipo.CADENA:
            if str(izq).lower() == str(der).lower() : return True
            return False

        self.tipo = Tipo.NULLO
        listaMensajes.append(Mensajes(3, "==", "Error TiposE", "no se permite comparar(==): \""+str(self.hijoIzq.tipo)+"\" con \"" +str(self.hijoDer.tipo)+"\" , solo int/double/string /boolean", self.fila, self.columna))
        return None


    # ***********************************************DIFERENCIACION ( != ).**********************************************************************************
    def diferenciacion_int(self,listaMensajes,izq,der):
        self.tipo = Tipo.BOOLEANO

        if self.hijoDer.tipo == Tipo.ENTERO:
            if int(izq) != int(der) : return True
            return False
        elif self.hijoDer.tipo == Tipo.DOBLE:
            if float(izq) != float(der) :   return True
            return False
        elif self.hijoDer.tipo == Tipo.CADENA:
            if str(izq) != str(der).lower() :   return True
            return False

        self.tipo = Tipo.NULLO
        listaMensajes.append(Mensajes(3, "(!=)", "De tipos ", "A. no se puede diferenciar un: "+str(self.hijoIzq.tipo)+" con un "+str(self.hijoDer.tipo), self.fila, self.columna))
        return None
    # ..............................................................................
    def diferenciacion_doble(self,listaMensajes,izq,der):
        self.tipo = Tipo.BOOLEANO

        if self.hijoDer.tipo == Tipo.ENTERO:        
            if float(izq) != float(der) : return True
            return False
        elif self.hijoDer.tipo == Tipo.DOBLE:
            if float(izq) != float(der) :   return True
            return False
        elif self.hijoDer.tipo == Tipo.CADENA:
            if str(izq) != str(der).lower() :   return True
            return False

        self.tipo = Tipo.NULLO
        listaMensajes.append(Mensajes(3, "(!=)", "De tipos ", "B. no se puede diferenciar un: "+str(self.hijoIzq.tipo)+" con un "+str(self.hijoDer.tipo), self.fila, self.columna))
        return None
    # ..............................................................................
    def diferenciacion_boolean(self,listaMensajes,izq,der):
        self.tipo = Tipo.BOOLEANO

        if self.hijoDer.tipo == Tipo.BOOLEANO or self.hijoDer.tipo == Tipo.CADENA:   
            if str(izq).lower() != str(der).lower() : return True
            return False
        self.tipo = Tipo.NULLO
        listaMensajes.append(Mensajes(3, "(!=)", "De tipos ", "C. no se puede diferenciar un: "+str(self.hijoIzq.tipo)+" con un "+str(self.hijoDer.tipo), self.fila, self.columna))
        return None
    # ..............................................................................
    def diferenciacion_char(self,listaMensajes,izq,der):
        if self.hijoDer.tipo == Tipo.CHAR:
            if str(izq).lower() != str(der).lower() : return True
            return False
        self.tipo = Tipo.NULLO
        listaMensajes.append(Mensajes(3, "(!=)", "De tipos ", "D. no se puede diferenciar un: "+str(self.hijoIzq.tipo)+" con un "+str(self.hijoDer.tipo), self.fila, self.columna))
        return None
    # ..............................................................................
    def diferenciacion_str(self,listaMensajes,izq,der):
        self.tipo = Tipo.BOOLEANO
        if self.hijoDer.tipo == Tipo.ENTERO or self.hijoDer.tipo == Tipo.DOBLE or self.hijoDer.tipo == Tipo.BOOLEANO or self.hijoDer.tipo == Tipo.CADENA:
            if str(izq).lower() != str(der).lower() :   return True
            return False
        self.tipo = Tipo.NULLO
        listaMensajes.append(Mensajes(3, "(!=)", "De tipos ", "E. no se puede diferenciar un: "+str(self.hijoIzq.tipo)+" con un "+str(self.hijoDer.tipo), self.fila, self.columna))
        return None

    # ***********************************************MENOR q ( < ).**********************************************************************************
    def menorQ_intDoble(self,listaMensajes,izq,der):
        self.tipo = Tipo.BOOLEANO
        if self.hijoDer.tipo == Tipo.ENTERO or self.hijoDer.tipo == Tipo.DOBLE:
            if float(izq) < float(der)  :   return True
            return False
        self.tipo = Tipo.NULLO
        listaMensajes.append(Mensajes(3, "(<)", "De tipos ", "A. no se puede buscar el MenoQ en un: "+str(self.hijoIzq.tipo)+" con un "+str(self.hijoDer.tipo), self.fila, self.columna))
        return None
    # ..............................................................................
    def menorQ_booleano(self,listaMensajes,izq,der):
        self.tipo = Tipo.BOOLEANO
        if self.hijoDer.tipo == Tipo.BOOLEANO:
            if int(izq) < int(der) : return True
            return False
        self.tipo = Tipo.NULLO
        listaMensajes.append(Mensajes(3, "(<)", "De tipos ", "B. no se puede buscar el MenoQ en un: "+str(self.hijoIzq.tipo)+" con un "+str(self.hijoDer.tipo), self.fila, self.columna))
        return None
    # ***********************************************MAYOR q ( < ).**********************************************************************************
    def mayorQ_intDoble(self,listaMensajes,izq,der):
        self.tipo = Tipo.BOOLEANO
        if self.hijoDer.tipo == Tipo.ENTERO or self.hijoDer.tipo == Tipo.DOBLE:
            if float(izq) > float(der)  :   return True
            return False
        self.tipo = Tipo.NULLO
        listaMensajes.append(Mensajes(3, "(>)", "De tipos ", "A. no se puede buscar el MayorQ en un: "+str(self.hijoIzq.tipo)+" con un "+str(self.hijoDer.tipo), self.fila, self.columna))
        return None
    # ..............................................................................
    def mayorQ_booleano(self,listaMensajes,izq,der):
        self.tipo = Tipo.BOOLEANO
        if self.hijoDer.tipo == Tipo.BOOLEANO:
            if int(izq) > int(der) : return True
            return False
        self.tipo = Tipo.NULLO
        listaMensajes.append(Mensajes(3, "(>)", "De tipos ", "B. no se puede buscar el MayorQ en un: "+str(self.hijoIzq.tipo)+" con un "+str(self.hijoDer.tipo), self.fila, self.columna))
        return None
    # ..............................................................................

    # ***********************************************MENOR IGUAL ( <= ).**********************************************************************************
    def menorIgual_intDoble(self,listaMensajes,izq,der):
        self.tipo = Tipo.BOOLEANO
        if self.hijoDer.tipo == Tipo.ENTERO or self.hijoDer.tipo == Tipo.DOBLE:
            if float(izq) <= float(der)  :   return True
            return False
        self.tipo = Tipo.NULLO
        listaMensajes.append(Mensajes(3, "(<=)", "De tipos ", "A. no se puede buscar el MenorIgual en un: "+str(self.hijoIzq.tipo)+" con un "+str(self.hijoDer.tipo), self.fila, self.columna))
        return None
    # ..............................................................................
    def menorIgual_booleano(self,listaMensajes,izq,der):
        self.tipo = Tipo.BOOLEANO
        if self.hijoDer.tipo == Tipo.BOOLEANO:
            if int(izq) <= int(der) : return True
            return False
        self.tipo = Tipo.NULLO
        listaMensajes.append(Mensajes(3, "(<=)", "De tipos ", "B. no se puede buscar el MenorIgual en un: "+str(self.hijoIzq.tipo)+" con un "+str(self.hijoDer.tipo), self.fila, self.columna))
        return None

    # ***********************************************MAYOR IGUAL ( >= ).**********************************************************************************
    def mayorIgual_intDoble(self,listaMensajes,izq,der):
        self.tipo = Tipo.BOOLEANO
        if self.hijoDer.tipo == Tipo.ENTERO or self.hijoDer.tipo == Tipo.DOBLE:
            if float(izq) >= float(der)  :   return True
            return False
        self.tipo = Tipo.NULLO
        listaMensajes.append(Mensajes(3, "(>=)", "De tipos ", "A. no se puede buscar el MayorIgual en un: "+str(self.hijoIzq.tipo)+" con un "+str(self.hijoDer.tipo), self.fila, self.columna))
        return None
    # ..............................................................................
    def mayorIgual_booleano(self,listaMensajes,izq,der):
        self.tipo = Tipo.BOOLEANO
        if self.hijoDer.tipo == Tipo.BOOLEANO:
            if int(izq) >= int(der) : return True
            return False
        self.tipo = Tipo.NULLO
        listaMensajes.append(Mensajes(3, "(>=)", "De tipos ", "B. no se puede buscar el MayorIgual en un: "+str(self.hijoIzq.tipo)+" con un "+str(self.hijoDer.tipo), self.fila, self.columna))
        return None
   
    # *********************************************** ---- (    ).**********************************************************************************
    # ..............................................................................
    # ..............................................................................
    # ..............................................................................
    # ..............................................................................
    # ..............................................................................
    # ..............................................................................
    # ..............................................................................





