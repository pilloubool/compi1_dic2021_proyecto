from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno
from Entorno.Simbolo import Simbolo
from graphviz import Digraph
from Retorno.Mensajes import Mensajes
from Instrucciones.FUNCION.Funcion import Funcion

class TablaSimbolos(Instruccion):
    def __init__(self, fila, columna,exp):
        self.fila = fila
        self.columna = columna
        self.exp = exp #nombre de la tabla de simbolos

    def ejecutar(self, ent:Entorno, listaMensajes):
        nombreTabSim = str(self.exp.ejecutar(ent,listaMensajes))
        
        dot = Digraph(comment='TablaSimbolos')
        toGraph = '''<<TABLE>
                <TR>
                <TD>Nom: {''' +nombreTabSim+'''}</TD>
                </TR>

                <TR>
                <TD>cont</TD>
                <TD>Tipo</TD>
                <TD>Nombre</TD>
                <TD>Valor</TD>
                </TR> \n'''

        #recorremos la tabla de simbolos capturando cada dato de su entorno
        listaSimbolos : Simbolo = ent.getTablaSimbolos()
        
        if len(listaSimbolos) != 0:# si el diccionario esta vacio
            cont = 0
            for claveSim in listaSimbolos:
                sim = listaSimbolos[claveSim]
                #si en la tabla de simbolos esta guardada una funcion, entonces extraemos el nomber de dicha funcion
                s = sim.valor
                if isinstance(s, Funcion):
                    s = str("Funcion: "+s.nombre)

                toGraph = toGraph + "<TR> <TD>"+str(cont)+ "</TD>" +"<TD>"+str(sim.tipo)+ "</TD>" +"<TD>"+str(sim.nombre)+ "</TD>" +"<TD>"+ str(s) + "</TD></TR>\n " 
                cont = cont + 1
            
            toGraph = toGraph + "</TABLE>>"

            dot.node('tabla', label=toGraph)

            #print(dot.source)
            dot.source
            dot.render('Reportes/'+ str(nombreTabSim), view=True)  # doctest: +SKIP    
            'Reportes/'+str(nombreTabSim)+'.pdf'
            #print("tabla: "+str(nombreTabSim))
        else:
            listaMensajes.append(Mensajes(4, "TablaSim", "\""+str(nombreTabSim)+"\"", "No hay variables declaradas, en este entorno ", self.fila, self.columna) )
