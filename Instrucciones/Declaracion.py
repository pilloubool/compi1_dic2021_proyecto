from Entorno.Entorno import Entorno
from Entorno.Simbolo import Simbolo
from Interface.Instruccion import Instruccion
from Retorno.Mensajes import Mensajes
from Entorno.Tipo import Tipo


class Declaracion(Instruccion):
    def __init__(self, fila, columna, identificador, tipo, expresion=None):
        self.fila = fila
        self.columna = columna
        self.identificador = identificador
        self.expresion = expresion
        self.tipo = tipo

    def ejecutar(self, entorno: Entorno,listaMensajes):
        banderaError = False
        auxTipo = Tipo.NULLO
        simbolo : Simbolo
        if self.expresion == None :
            simbolo = Simbolo(self.tipo, str(self.identificador).lower(), None, self.fila)
        elif self.expresion == Tipo.NULLO:
            simbolo = Simbolo(self.tipo, str(self.identificador).lower(),Tipo.NULLO, self.fila)
        else:
            valor = self.expresion.ejecutar(entorno,listaMensajes) #capturando el valor de la exprecion
            auxTipo = self.expresion.tipo
            # verificar que valor no sea un error o nulo (None)
            if valor != None: #se crea un nuevo simbolo
                simbolo = Simbolo(self.tipo, str(self.identificador).lower(), valor, self.fila)
            else : 
                banderaError = True
                listaMensajes.append(Mensajes(6, "Declaracion", "Exp = None", "La resolucion de la Exprecion (E), No existe o es Null", self.fila, self.columna))
           
            #----------------------------------------------VERIFICACION DE TIPOS .------------------------------------------------------
        if self.tipo ==  auxTipo or self.expresion == Tipo.NULLO or self.expresion ==None:  #tipos iguales o fue declarado con NULL
            if not banderaError:    #no hay error en la creacion del nuevo Simbolo
                declaracion = None
                if entorno.buscarSimbolo(simbolo.nombre)==None:
                    declaracion = entorno.nuevoSimbolo(simbolo)     #el nuevo simbolo se agrega al entorno

                if declaracion == None:     #Error en declaracion
                    listaMensajes.append(Mensajes(6, "Declaracion", str(self.tipo)+" "+str(self.identificador)+" = Error", "No se pudo declarar la variable: \""+str(self.identificador)+"\", por que ya existe.", self.fila  , self.columna))
        else: # si existe Error de tipos
            listaMensajes.append(Mensajes(3, "Declaracion", "Error de Tipos", "A la var: \""+str(self.identificador)+" ("+str(self.tipo) +")\", No se puede asignar una Exp de ("+str(auxTipo)+")", self.fila, self.columna))