from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno
from Entorno.Tipo import Tipo

from Entorno.Simbolo import Simbolo
from Entorno.Tipo import Tipo
from Instrucciones.ARREGLOS.ArregloInstancia import ArregloInstancia
from Instrucciones.ARREGLOS.ArregloLista import ArregloLista
from Retorno.Mensajes import Mensajes

class modificacionArreglo(Instruccion):
    def __init__(self, fila, columna,nombre,lexp,datoModificacion):
        self.fila       = fila
        self.columna    = columna
        self.nombre     = nombre
        self.lexp       = lexp 
        self.datoModificacion = datoModificacion
        self.coordenadas = []
        self.arreglo = []
        self.tipo = Tipo.NULLO

    def ejecutar(self, ent:Entorno, listaMensajes):
        self.coordenadas.clear()
        nombreInstruccion = "Modificacion_Arreglo"
        #comprobamos que la variable exista
        sim:Simbolo = ent.buscarSimbolo(self.nombre)
        if sim is not None:
            claseArreglo = sim.valor
            if isinstance(claseArreglo, ArregloLista) or isinstance(claseArreglo, ArregloInstancia):
                #comprobamos que el arreglo guardado y la modificacion, tengan la misma profundidad/dimensiones
                if claseArreglo.cantDim == len(self.lexp):

                    #extraemos las coordenadas
                    for exp in self.lexp:
                        retorno = exp.ejecutar(ent,listaMensajes)
                        if exp.tipo != Tipo.ENTERO:
                            self.tipo = Tipo.NULLO
                            listaMensajes.append(Mensajes(3, nombreInstruccion, "De tipos", "arreglo:\" "+str(self.nombre)+" \", en las dimensiones solo se permite INT, se envio ("+str(exp.tipo)+")", self.fila, self.columna))
                            return Tipo.NULLO
                        self.coordenadas.append(retorno)

                    #buscamos/retornamos el nodo, con las coordenadas
                    self.arreglo = claseArreglo.listaDedimensiones
                    #nodo = self.buscarCoordenada(0, self.coordenadas, self.arreglo, listaMensajes)
                    #ejecutamos el valor a moficicar y comprobamos que sea del mismo tipo que el arreglo
                    toKeep = self.datoModificacion.ejecutar(ent,listaMensajes)
                    if self.datoModificacion.tipo == claseArreglo.tipo:
                        #nodo sera de tipo primitivo, se modifica su valor, con la enviada en el constructor
                        self.tipo = claseArreglo.tipo
                        nodo = None
                       # nodo.valor = toKeep
                        if isinstance(claseArreglo, ArregloInstancia):
                            claseArreglo.modificacionArreglo(0, self.coordenadas, self.arreglo, listaMensajes,toKeep)
                        elif isinstance(claseArreglo, ArregloLista):
                            nodo = self.buscarCoordenada(0, self.coordenadas, self.arreglo, listaMensajes)
                            nodo.valor = toKeep
                        print("")
                    else :
                        listaMensajes.append(Mensajes(3, nombreInstruccion, "De tipos", "El arreglo: \""+str(self.nombre)+"("+str(claseArreglo.tipo)+")\" , no admite ("+str(self.datoModificacion.tipo)+")", self.fila, self.columna))
                        self.tipo = Tipo.NULLO
                        return Tipo.NULLO
                else:
                    listaMensajes.append(Mensajes(3, nombreInstruccion, "De dimensiones", "las dimensiones (cant corchetes), no concuerdan con el arreglo guardado: \""+str(self.nombre)+"\"  ", self.fila, self.columna))
            # listaDedimensiones    cantDim
            else :
                listaMensajes.append(Mensajes(3, nombreInstruccion, "De tipos", "La variable: \""+str(self.nombre)+"\", No es de Tipo ARREGLO, es ("+str(sim.tipo)+")", self.fila, self.columna))
        else :
            listaMensajes.append(Mensajes(3, nombreInstruccion, "Declaracion", "No existe el Arreglo: \""+str(self.nombre)+"\"  ", self.fila, self.columna))
            return Tipo.NULLO



    def buscarCoordenada(self,cont,lcoordenadas,arreglo,listaMensajes):
        if isinstance(arreglo, list):
            pos = lcoordenadas[cont] #por que las coordenadas, las manejares desde 0 (cero), y la expresion nos retorna desde 1
            #comprobamos que el arreglo cuente con un tamaño que este dentro de la coordenada
            if pos < len(arreglo):
                return self.buscarCoordenada(cont + 1 , lcoordenadas, arreglo[pos],listaMensajes)  
            else : 
                #retorno de Error, por que se quiere acceder, a una posicion del arreglo, que no existe 
                listaMensajes.append(Mensajes(3, "Modificacion_Arreglo", "Dimensiones", "se busca una dimension que esta fuera de los limites del arreglo: \""+str(self.nombre)+"\" ", self.fila, self.columna))            
                return None
        else : 
            return arreglo # retornara el ultimo dato del arreglo (dato tipo primitivo,data buscado)
