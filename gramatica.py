import ply.yacc as yacc
import ply.lex as lex
from Expresiones.Aritmetica import Aritmetica
from Expresiones.Primitivo import Primitivo
from Expresiones.Identificador import Identificador
from Instrucciones.Imprimir import Imprimir
from Instrucciones.Declaracion import Declaracion
from Entorno.Entorno import Entorno
from Interface.Instruccion import Instruccion
from Instrucciones.Asignacion import Asignacion
from Interface.Error import Error
from Retorno.Mensajes import Mensajes
from Entorno.Simbolo import Simbolo
from Entorno.Tipo import Tipo
from Expresiones.Unarias import Unarias
from Expresiones.Relacionales import Relacionales
from Expresiones.Logicas import Logicas
from Expresiones.IncrementoDecremento import IncrementoDecremento
from Instrucciones.IF.IF import IF
from Instrucciones.IF.ELSEIF import ELSEIF
from Instrucciones.IF.ELSE import ELSE
from Instrucciones.While import While
from Instrucciones.DoWhile import DoWhile
from Instrucciones.FOR import FOR
from Instrucciones.TablaSimbolos import TablaSimbolos
from Expresiones.Casteo import Casteo
from Instrucciones.SwitchCase.SwitchCase import SwitchCase
from Instrucciones.SwitchCase.Case import Case
from Instrucciones.SwitchCase.Default import Default
from Instrucciones.Break import Break
from Instrucciones.Return import Return
from Instrucciones.ARREGLOS.ArregloInstancia import ArregloInstancia
from Instrucciones.FUNCION.Parametro import Parametro
from Instrucciones.FUNCION.Funcion import Funcion
from Instrucciones.FUNCION.LlamadaFuncion import LlamadaFuncion
from Instrucciones.FuncionesNativas import FuncionesNativas
from Instrucciones.ARREGLOS.ArregloLista import ArregloLista
from Instrucciones.ARREGLOS.LLamadaArreglo import LLamadaArreglo
from Instrucciones.ARREGLOS.modificacionArreglo import modificacionArreglo






reservadas = {
    'while'     :   'while',
    'switch'    :   'switch',
    'expr'      :   'expr',
    'case'      :   'case',
    'default'   :   'default',
    'break'     :   'break',
    'return'    :   'return',

    'new'       :   'new',

    'func'      :   'func',
    'nombre'    :   'nombre',
    'param'     :   'param',
    'void'      :   'void',
    'tipo'      :   'tipo',

    'do'        :   'do',
    'for'       :   'for',
    'cond'      :   'cond',
    'instr'     :   'instr',

    'int'       :   'r_int',
    'double'    :   'r_double',
    'char'      :   'r_char',
    'string'    :   'r_string',
    'boolean'   :   'r_boolean',
    'true'      :   'r_true',
    'false'     :   'r_false',
    'null'      :   'r_null',

    'if'        :   'r_if',
    'else'      :   'r_else',

    'tablasimbolos' :   'tablasimbolos',
    'imprimir'  : 'imprimir',
    'aminus'    :   'aminus',
    'amayus'    :   'amayus',
    'truncate'  :   'truncate',
    'round'     :   'round',
    'tamano'    :   'tamano'



}

t_dosp = r':'
t_pyc = r';'
t_llavea = r'{'
t_llavec = r'}'
t_corchetea = r'\['
t_corchetec = r'\]'
t_para = r'\('
t_parc = r'\)'
t_coma = r','
t_mas = r'\+'
t_menos = r'-'
t_or = r'\|\|'
t_and = r'&&'
t_not = r'!'
t_igualigual = r'=='
t_diferente = r'!='
t_menorque = r'<'
t_menorigual = r'<='
t_mayorque = r'>'
t_mayorigual = r'>='
t_dividido = r'/'
t_por = r'\*'
t_modulo = r'%'
t_potencia = r'\*\*'
t_masmas = r'\+\+'
t_menosmenos = r'--'
t_comillaSimple = r'\''
t_comillasD = r'\"'

tokens = [
    'dosp',
    'pyc',
    'llavea',
    'llavec',
    'para',
    'parc',
    'coma',
    'mas',
    'menos',
    'or',
    'and',
    'not',
    'igualigual',
    'diferente',
    'menorque',
    'menorigual',
    'mayorque',
    'mayorigual',
    'dividido',
    'por',
    'modulo',
    'potencia',
    'masmas',
    'menosmenos',
    'comillaSimple',
    'comillasD',
    'corchetea',
    'corchetec',

    'int',
    'double',
    'char',
    'cadenaString',
    'boolean',
    'id',
    'null',


] + list(reservadas.values())

# Comentario multilinea 

def t_MLCOMMENT(t):
    r'\#&(.|\n)*?&\#'
    #listaMensajes.append(Mensajes(4, "imp", "Coment Multi", "msj", t.lexer.lineno, len(str(t.value))))
    t.lexer.lineno += t.value.count('\n')

def t_SLCOMMENT(t):
    r'\#[^\*].*?\n'
    #listaMensajes.append(Mensajes(4, "imp", "Coment Simple", "msj", t.lexer.lineno, len(str(t.value))))
    t.lexer.lineno += 1

def t_double(t):
    r'\d+\.\d+'
    try:
        t.value = float(t.value)
    except ValueError:
        listaMensajes.append(Mensajes(6, "", "", "Valor numerico (Double) <"+t.value+">  incorrecto", t.lexer.lineno, t.lexpos))
        t.value = 0        
    return t


def t_int(t):
    r'\d+'
    try:
        t.value = int(t.value)
    except ValueError:
        print("Valor numerico incorrecto %d", t.value)
        listaMensajes.append(Mensajes(6, "", "", "Valor numerico (Int) <"+t.value+">  incorrecto", t.lexer.lineno, t.lexpos))
        t.value = 0
    return t


def t_cadenaString(t):
    r'"(.|\n|\r|_|\t|\\\'|\\\\)*?"'
    t.value = t.value[1:-1]  # remuevo las comillas
    return t


def t_id(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reservadas.get(t.value.lower(), 'id')
    return t

def t_char(t):
    r'\'([^\n\r\\]|[a-zA-Z0-9]|\\n|\\N|\\r|\\R|\\t|\\T|\\"|\\\'|\\\\)\''
    t.value = t.value[1:-1] # removes apostrophe
    t.value = t.value.replace('\\\\','\\')
    t.value = t.value.replace('\\n','\n')
    t.value = t.value.replace('\\N','\n')
    t.value = t.value.replace('\\t','\t')
    t.value = t.value.replace('\\T','\t')
    t.value = t.value.replace('\\r','\r')
    t.value = t.value.replace('\\R','\r')
    t.value = t.value.replace('\\"','\"')
    t.value = t.value.replace('\\\'','\'')
    t.value = t.value.lower()
    return t


t_ignore = " \t\r"

# Compute column.
#     input is the input text string
#     token is a token instance


def find_column(inp, token):
    '''line_start = inp.rfind('\n', 0, token.lexpos) + 1
    return (token.lexpos - line_start) + 1'''
    return 1


def t_newline(t):
    r'\n+'
    t.lexer.lineno += t.value.count("\n")


def t_error(t):
    print("Caracter invalido '%s'" % t.value[0])
    listaMensajes.append(
        Error("Léxico", "Caracter invalido '%s'" % t.value[0], 0, 0))
    t.lexer.skip(1)




lexer = lex.lex()

# precedencia de operadores

precedence = (
    ('left', 'or'),
    ('left', 'and'),
    ('right', 'not'),
    ('left', 'igualigual', 'diferente', 'menorque',
     'menorigual', 'mayorque', 'mayorigual'),
    ('left', 'mas', 'menos'),
    ('left', 'dividido', 'por', 'modulo'),
    ('nonassoc', 'potencia'),
    ('left', 'masmas', 'menosmenos'),
    ('right', 'negativo'),
    ('right', 'casteo')
)


# definición de la gramática

#   ***********************************   instrucciones************************

def p_init(t):
    'INIT : LINSTRUCCIONES'
    t[0] = t[1]


def p_linstrucciones(t):
    'LINSTRUCCIONES : LINSTRUCCIONES INSTRUCCIONES'
    t[1].append(t[2])
    t[0] = t[1]


def p_linstrucciones2(t):
    'LINSTRUCCIONES : INSTRUCCIONES'
    t[0] = [t[1]]


def p_instrucciones(t):
    '''INSTRUCCIONES : WHILE
                    | DECL
                    | ASIGNACION
                    | IF
                    | INCRDECR pyc
                    | DOWHILE
                    | FOR
                    | SWITCH_INSTR
                    | BREAK_INST
                    | RETURN_INST
                    | ARREGLOS
                    | FUNCION
                    | FUNCIONES_NATIVAS
                    '''
    t[0] = t[1]


def p_asignacion(t):
    'ASIGNACION : id dosp E pyc'
    t[0] = Asignacion(t.lineno(1), find_column(
        input, t.slice[1]), str(t[1]), t[3])


def p_asignacionError(t):
    'ASIGNACION : id dosp error pyc'
    listaMensajes.append(Error("Sintáctico", "Se esperaba una expresión en la asignación de variables", 0, 0))


def p_decl(t):
    'DECL : TIPO id dosp E pyc'
    t[0] = Declaracion(t.lineno(1), find_column(
        input, t.slice[1]), str(t[2]), t[1], t[4])


def p_decl2(t):
    'DECL : TIPO id pyc'
    t[0] = Declaracion(t.lineno(1), find_column(input, t.slice[1]), str(t[2]), t[1],None)




#  ********************************************************************************** FUNCIONES NATIVAS *********************************************************************************************************************

def p_funcNativas(t):
    '''FUNCIONES_NATIVAS    :   TSIMBOLOS
                            |   IMPRIMIR
                            |   OTRAS_NATIVAS pyc'''
    t[0]    =   t[1]
#.....................................................

def p_imprimir(t):
    'IMPRIMIR : imprimir para E parc pyc'
    t[0] = Imprimir(t.lineno(1), find_column(
        input, t.slice[1]), t[3])

#.....................................................
def p_tabSIMBOLOS(t):
    'TSIMBOLOS :   tablasimbolos para E parc pyc'
    t[0] = TablaSimbolos(0, 0,t[3])

#.....................................................

def p_otrasNativas(t):
    'OTRAS_NATIVAS  :   TIPO_NATIVA para E parc'
    t[0]    =   FuncionesNativas(t.lineno(1), find_column(input, t.slice[1]),t[1],t[3])
#.....................................................
def p_tipoNativa(t):
    '''TIPO_NATIVA      :   aminus
                        |   amayus
                        |   truncate
                        |   round
                        |   tamano'''
    t[0]    =   t[1]
#.....................................................

# ((((((((((((((((((()))))))))))))))))))    SENTENCIAS DE CONTROL //((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))

def p_cond(t):
    'COND : cond dosp para E parc'
    t[0] = t[4]
#......................................................

def p_instr(t):
    'INSTR : instr dosp llavea LINSTRUCCIONES llavec'
    t[0] = t[4]

#......................((((((  IF   ))))))................................
def p_instIF(t):
    'IF   :   r_if dosp llavea   COND coma   INSTR  llavec'
    t[0] = IF(t.lineno(1), find_column(input, t.slice[1]),t[4],t[6],None,None)

def p_instIF2(t):
    'IF   :   r_if dosp llavea   COND coma   INSTR  llavec ELSE'
    t[0] = IF(t.lineno(1), find_column(input, t.slice[1]),t[4],t[6],None,t[8])

def p_instIF3(t):
    'IF   :   r_if dosp llavea   COND coma   INSTR  llavec LELSEIF'
    t[0] = IF(t.lineno(1), find_column(input, t.slice[1]),t[4],t[6],t[8],None)

def p_instIF4(t):
    'IF   :   r_if dosp llavea   COND coma   INSTR  llavec LELSEIF ELSE'
    t[0] = IF(t.lineno(1), find_column(input, t.slice[1]),t[4],t[6],t[8],t[9])


def p_linstELSEIF1(t):#elseIF
    'LELSEIF   :   LELSEIF ELSEIF'
    t[1].append(t[2])
    t[0] = t[1]

def p_linstElSEIF2(t):#elseIF
    'LELSEIF    :  ELSEIF'
    t[0] = [t[1]]

def p_instELSEIF3(t):#elseIF
    'ELSEIF :   r_else r_if dosp llavea COND coma INSTR llavec'
    t[0]    =   ELSEIF(t.lineno(1), find_column(input, t.slice[1]),t[5],t[7])

def p_instELSE(t):#else
    'ELSE   :   r_else dosp llavea INSTR llavec'
    t[0] = ELSE(t.lineno(1), find_column(input, t.slice[1]),t[4])

#......................((((((  SWITCH CASE   ))))))................................ DEFAULTSWITCH


def p_switch1(t):
    'SWITCH_INSTR : switch dosp llavea expr dosp E coma LCASES DEFAULT_INS llavec'
    t[0]    =   SwitchCase(t.lineno(1), find_column(input, t.slice[1]), t[6], t[8], t[9])


def p_switch2(t):
    'SWITCH_INSTR : switch dosp llavea expr dosp E coma LCASES llavec'
    t[0]    =   SwitchCase(t.lineno(1), find_column(input, t.slice[1]), t[6], t[8], None)


def p_switch3(t):
    'SWITCH_INSTR : switch dosp llavea expr dosp E coma DEFAULT_INS llavec'
    t[0]    =   SwitchCase(t.lineno(1), find_column(input, t.slice[1]), t[6], None, t[8])


def p_lcases1(t):
    'LCASES : LCASES coma CASES'
    t[1].append(t[3])
    t[0] = t[1]


def p_lcases2(t):
    'LCASES : CASES'
    t[0] = [t[1]]


def p_cases(t):
    'CASES : case dosp llavea expr dosp E coma INSTR llavec'
    t[0]    =   Case(t.lineno(1), find_column(input, t.slice[1]), t[6], t[8])


def p_default(t):
    'DEFAULT_INS : default dosp llavea INSTR llavec'
    t[0]    =   Default(t.lineno(1), find_column(input, t.slice[1]), t[4])

def p_default2(t):
    'DEFAULT_INS : coma default dosp llavea INSTR llavec'
    t[0]    =   Default(t.lineno(1), find_column(input, t.slice[1]), t[5])


#   *********************************** *******************  CICLOS ********************************************************


#......................((((((  while   ))))))................................

def p_while(t):
    'WHILE : while dosp llavea COND coma INSTR llavec'
    t[0] = While(t.lineno(1), find_column(input, t.slice[1]), t[4], t[6])

#......................((((((  do while   ))))))................................
def p_DoWhile(t):
    'DOWHILE    :   do dosp llavea  INSTR  coma while dosp llavea COND llavec llavec'
    t[0] = DoWhile(t.lineno(1), find_column(input, t.slice[1]),t[4],t[9])

#......................((((((  FOR   ))))))................................
def p_FOR(t):
    'FOR    :   for dosp llavea             cond dosp para CONDFOR E pyc E  parc                    coma  INSTR llavec '
    t[0] = FOR(t.lineno(1), find_column(input, t.slice[1]),t[7],t[8],t[10],t[13])

def p_FOR2(t):
    '''CONDFOR  :   ASIGNACION
                |   DECL'''
    t[0] = t[1]

#   ***********************************   INCREMENTO DECREMENTO ***************************************************************

def p_incrDecr(t):
    '''INCRDECR : id masmas
                | id menosmenos'''
    t[0] = IncrementoDecremento(t.lineno(1), find_column(input, t.slice[1]),t[1],t[2])

def p_eIncDec(t):
    'E : INCRDECR '
    t[0] = t[1]




def p_incrDecrError(t):
    'INCRDECR : error masmas'
#   ***********************************   E    *********************************************************************************


def p_tipo(t):
    '''TIPO : r_string
    | r_int
    | r_double
    | r_boolean
    | r_char'''
    if str(t[1]).lower() == "string":
        t[0] = Tipo.CADENA
    elif str(t[1]).lower() == "int":
        t[0] = Tipo.ENTERO
    elif str(t[1]).lower() == "double":
        t[0] = Tipo.DOBLE
    elif str(t[1]).lower() == "boolean":
        t[0] = Tipo.BOOLEANO
    elif str(t[1]).lower() == "char":
        t[0] = Tipo.CHAR


def p_eAritmetica(t):
    '''E : E mas E 
        | E menos E
        | E por E
        | E dividido E
        | E potencia E
        | E modulo E'''
    t[0] = Aritmetica(t.lineno(1), find_column(input, t.slice[1]), str(t[2]), t[1], t[3])


def p_eLogicas(t):
    '''E : E and E
        | E or E
    '''
    t[0] = Logicas(t.lineno(1), find_column(input, t.slice[1]), t[2], t[1],t[3])


def p_eRelacional(t):
    '''E : E menorque E
        | E mayorque E
        | E menorigual E
        | E mayorigual E
        | E igualigual E
        | E diferente E 
    '''
    t[0] = Relacionales(t.lineno(1), find_column(input, t.slice[1]), t[2], t[1], t[3])


def p_eUnarias(t):
    '''E    :   not E
            |   menos E %prec negativo'''
    t[0] = Unarias(t.lineno(1), find_column(input, t.slice[1]), t[1], t[2])


def p_eAgrupacion(t):
    'E : para E parc'
    t[0] = t[2]

def p_e(t):
    'E : int'
   
    t[0] = Primitivo(t.lineno(1), find_column(
        input, t.slice[1]), Tipo.ENTERO, int(t[1]))

def p_e2(t):
    'E : double'
    t[0] = Primitivo(t.lineno(1), find_column(
        input, t.slice[1]), Tipo.DOBLE,float(t[1]))

def p_e3T(t):
    'E : r_true'
    t[0] = Primitivo(t.lineno(1), find_column(
        input, t.slice[1]), Tipo.BOOLEANO, True)

def p_e3F(t):
    'E : r_false'
    t[0] = Primitivo(t.lineno(1), find_column(
        input, t.slice[1]), Tipo.BOOLEANO, False)
    
def p_e4(t):
    'E : char'
    t[0] = Primitivo(t.lineno(1), find_column(
        input, t.slice[1]), Tipo.CHAR, str(t[1]))

def p_e5(t):
    'E : cadenaString'
    t[0] = Primitivo(t.lineno(1), find_column(
        input, t.slice[1]), Tipo.CADENA, str(t[1]))


def p_eId(t):
    'E : id'
    t[0] = Identificador(t.lineno(1), find_column(
        input, t.slice[1]), str(t[1]))

def p_ellamadaFuncion(t):
    'E  :   LLAMADAFUNCION'
    t[0]    =   t[1]

def p_eFuncionesNativas(t):
    'E  :   OTRAS_NATIVAS'
    t[0] = t[1]
 

def p_eNull(t):
    'E  :   r_null'
    t[0] = Tipo.NULLO

#llamada de un arreglo, tipo expresion
#..........................................................
def p_eLLamadaArreglo(t):
    'E  :   LLAMADA_ARREGLO'
    t[0]    =   t[1]

#   ***********************************   CASTEO    *********************************************************************************
def p_casteo(t):
    'E  :   corchetea    TIPO    corchetec  E %prec casteo'
    t[0] = Casteo(t.lineno(1), find_column(
        input, t.slice[1]), t[2],t[4])


#   ***********************************   SENTENCIAS DE TRANSFERENCIA    *********************************************************************************
def p_instBreak(t):
    'BREAK_INST  :   break pyc'
    t[0]    =   Break(t.lineno(1), find_column(input, t.slice[1]))

def p_instReturn(t):
    'RETURN_INST    :   return E pyc'
    t[0]    =   Return(t.lineno(1), find_column(input, t.slice[1]),t[2])

def p_instReturn2(t):
    'RETURN_INST    :   return  pyc'
    t[0]    =   Return(t.lineno(1), find_column(input, t.slice[1]),None)

#   ***********************************   ARREGLOS    *********************************************************************************
def p_Arreglos(t):
    '''ARREGLOS     :   DECLARACION_ARREGLO_INSTANCIA
                    |   DECLARACION_ARREGLO_LISTA
                    |   LLAMADA_ARREGLO pyc
                    |   MODIFICACION_ARREGLO
                    '''
    t[0]    =   t[1]

# Declaracion por Instancia
#..........................................................
def p_arregloInstancia(t):
    'DECLARACION_ARREGLO_INSTANCIA   :   TIPO    CONTEODIMENCIONES    id   dosp    new     TIPO    LDIMEXP     pyc'
    t[0]    =   ArregloInstancia(t.lineno(1), find_column(input, t.slice[1]),t[1],t[2],t[3],t[6],t[7])

def p_CONTEODIMENCIONES(t):
    'CONTEODIMENCIONES    :   CONTEODIMENCIONES corchetea corchetec'
    t[0]    =   t[1]    +   1

def p_corchetess(t):
    'CONTEODIMENCIONES     :   corchetea corchetec'
    t[0]    =   1

def p_lDimExp1(t):
    'LDIMEXP    :   LDIMEXP     corchetea   E   corchetec'
    t[1].append(t[3])
    t[0]    =   t[1]

def p_lDimExp2(t):
    'LDIMEXP    :   corchetea   E   corchetec'
    t[0]    =   [t[2]]

#Declaracion por Lista
#..........................................................
def p_declaracionArregloPorLista(t):
    'DECLARACION_ARREGLO_LISTA  :   TIPO CONTEODIMENCIONES id dosp     LISTADO_VALORES_ARREGLO_T2    pyc'
    t[0]    =   ArregloLista(t.lineno(1), find_column(input, t.slice[1]),t[1],t[2],t[3],t[5])

    #simple
def p_listadoValoresArregloT2(t):
    'LISTADO_VALORES_ARREGLO_T2 :   llavea LEXP llavec'
    t[0]    =   t[2]

    #recursivo
def p_listadoValoresArregloT2_recursivo(t):
    'LISTADO_VALORES_ARREGLO_T2 :   llavea LARR_RECURSIVO llavec'
    t[0]    =   t[2]

def p_listadoValoresArregloT2_recursivo2(t):
    'LARR_RECURSIVO :   LARR_RECURSIVO coma LISTADO_VALORES_ARREGLO_T2'
    t[1].append(t[3])
    t[0] = t[1]

def p_listadoValoresArregloT2_recursivo3(t):
    'LARR_RECURSIVO :   LISTADO_VALORES_ARREGLO_T2'
    t[0]    =   [t[1]]

#llamada
#..........................................................
def p_llamadaArreglo(t):
    'LLAMADA_ARREGLO    :   id LDIMEXP'
    t[0]    =   LLamadaArreglo(t.lineno(1), find_column(input, t.slice[1]),t[1],t[2])


#Modificacion   (self, fila, columna,tipo,cantDim,nombreArreglo,tipoAsignacion,LexpDim):
#..........................................................
def p_modificacionArreglo(t):
    'MODIFICACION_ARREGLO   :   id LDIMEXP dosp E pyc'
    t[0]    =   modificacionArreglo(t.lineno(1), find_column(input, t.slice[1]),t[1],t[2],t[4])
#..........................................................


#   ***********************************   FUNCIONES    *********************************************************************************

def p_funciones(t):
    '''FUNCION  :   DECFUNCION
                |   LLAMADAFUNCION pyc'''
    t[0]    =   t[1]

def p_funciones2(t):
    'DECFUNCION    :   func dosp llavea       tipo dosp TIPOFUNCION coma       nombre dosp id coma      param dosp corchetea LISTAPARAM corchetec coma       INSTR       llavec'
    t[0]    =   Funcion(t.lineno(1), find_column(input, t.slice[1]), t[6], t[10], t[15], t[18])

def p_tipoFuncion(t):
    '''TIPOFUNCION  :   void
                    |   TIPO'''
    if str(t[1]).lower() == "void":
        t[0] = Tipo.VOID
    else:   t[0] = t[1]

def p_listaParametros1(t):
    'LISTAPARAM     :   LISTAPARAM coma PARAMETRO'
    t[1].append(t[3])
    t[0]    =   t[1]

def p_listaParametros2(t):
    'LISTAPARAM     :   PARAMETRO'
    t[0]    =  [t[1]] 

def p_listaParametros3(t):
    'PARAMETRO      :   TIPO id'
    t[0]    =   Parametro(t.lineno(1), find_column(input, t.slice[1]), t[1],t[2])


def p_listaParametros4(t):
    'PARAMETRO      :   TIPO  CONTEODIMENCIONES id'
    t[0]    =   ArregloInstancia(t.lineno(1), find_column(input, t.slice[1]),t[1],t[2],t[3],None,None)




def p_llamadaFuncion(t):#tipo instruccion
    'LLAMADAFUNCION :  id para LEXP parc'
    t[0]    =   LlamadaFuncion(t.lineno(1), find_column(input, t.slice[1]),t[1],t[3])


def p_lexpparm(t):#lista de expresiones de parametros
    'LEXP   :   LEXP coma E'
    t[1].append(t[3])
    t[0]    = t[1]

def p_paramllamada(t):
    'LEXP     :   E'
    t[0]    = [t[1]]





#   ***********************************   ERROR    *********************************************************************************

def p_error(t):
    print("Error sintáctico en '%s'" % t.value)


parser = yacc.yacc()
listaMensajes = []


def analizar(data):
    global listaMensajes 
    listaMensajes.clear()
    entrada = '''
    imprimir("prueba de fuego");
    imprimir(5+8);
    int nota : 61;
    imprimir(nota);
    string variable1 : "Hola esta es una prueba";
    imprimir(variable1);
    nota : 100;
    imprimir(nota);
    imprimir("Nuestra nota final de compi1 es: " + nota);
    '''
    instrucciones = parser.parse(data)
    entornoGlobal = Entorno(None)
    for ins in instrucciones:
            valor = ins.ejecutar(entornoGlobal,listaMensajes)
 
    return listaMensajes

#analizar()
