from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno
from Retorno.Mensajes import Mensajes
from Instrucciones.Break import Break
from Instrucciones.Return import Return



class While(Instruccion):
    def __init__(self, fila, columna, condicion, instrucciones):
        self.condicion = condicion
        self.instrucciones = instrucciones
        self.fila = fila
        self.columna = columna

    def ejecutar(self, entPadre: Entorno,listaMensajes):


        while(True):
            entWhile = Entorno(entPadre)
            cond = self.condicion.ejecutar(entWhile,listaMensajes) #cond debera ser boolean para funcionar
            if isinstance(cond, bool):
                if cond:
                    for ins in self.instrucciones:
                        retorno =   ins.ejecutar(entWhile,listaMensajes)
                        if isinstance(retorno,Break):
                            return self # al aparecer break, retornaremos el propio while, para salir unicamente de este ciclo y no de otros
                        elif isinstance(retorno, Return):
                            return retorno
                else : break
            else:
                listaMensajes.append(Mensajes(3, " WHILE ", "Exp no booleana", "condicion solo acepta Exp.Booleanas (permitidas)", self.fila, self.columna))
                break
