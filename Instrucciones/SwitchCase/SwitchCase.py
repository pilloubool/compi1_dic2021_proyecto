from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno
from Instrucciones.Break import Break
from Instrucciones.SwitchCase.Case import Case

class SwitchCase(Instruccion):
    def __init__(self, fila, columna,expSwitch,Lcase,Default):
        self.fila = fila
        self.columna = columna
        self.expSwitch = expSwitch
        self.Lcase = Lcase
        self.Default = Default

    def ejecutar(self, entPadre, listaMensajes):
        valorIdSwitch = self.expSwitch.ejecutar(entPadre,listaMensajes)
        entCase = Entorno(entPadre)
        seEncontroBreak = 0
        '''
        seEncontroBreak 
            0   =   NO se encontro BREAK y NO se a ejecutado ningun CASE, estado inicial
            1   =   si se encontro un BREAK en la ejecucion del case, NO ejecutamos DEFAULT
            2   =   si se ejecuto el CASE pero No se encontro un BREAK, entonces seguimos ejecutando los siguientes CASES
        '''

        if self.Lcase is not None: #comprobando que exista una lista de case, para analizar
            for case in self.Lcase:
                retorno = case.ejecutar(entCase,listaMensajes,valorIdSwitch,seEncontroBreak)
                if isinstance(retorno, Break): #buscando instruccion BREAK,si no se encuentra seguimos con el otro CASE
                        seEncontroBreak = 1
                        break
                elif isinstance(retorno,Case):#si se ejecuto el case y este no tiene break, entonces ejecutamos  el siguiente caso
                    if retorno.seEjecutoEsteCase:
                        seEncontroBreak = 2

        if self.Default is not None and  not seEncontroBreak == 1: # comprobando que exita un Default para analizar
            self.Default.ejecutar(entCase,listaMensajes)


