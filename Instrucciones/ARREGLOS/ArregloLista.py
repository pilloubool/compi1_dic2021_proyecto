from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno
from Entorno.Simbolo import Simbolo
from Retorno.Mensajes import Mensajes
from Instrucciones.ARREGLOS.ArregloInstancia import ArregloInstancia

class ArregloLista(Instruccion):
    def __init__(self, fila, columna,tipo,cantDim,nombre,listaDedimensiones):
        self.fila = fila
        self.columna = columna
        self.tipo = tipo
        self.cantDim = cantDim
        self.nombre = nombre
        self.listaDedimensiones = listaDedimensiones

    def ejecutar(self, entorno:Entorno, listaMensajes):
        if entorno.buscarSimbolo(self.nombre) == None:
            cantDimEnLista = self.comprobandoProfundidadArreglo(0,self.listaDedimensiones)
            #comprobar que la profundidad de la lista, sea igual a ala CantDim declarada
            if cantDimEnLista == self.cantDim:
                entorno.nuevoSimbolo(Simbolo(self.tipo,self.nombre,self,self.fila))
            else:
                listaMensajes.append(Mensajes(3, "Decl Arreglo Lista(T2)", "Declaracion", "en la declaracion del arreglo:\""+str(self.nombre)+"\", las dimenciones no concuerdan(cantidad de corchetes IZQ no = DER).", self.fila, self.columna))
        else : 
            listaMensajes.append(Mensajes(3, "Decl Arreglo Lista(T2)", "Declaracion", "No se puede declarar/guardar el arregloT2 :\""+str(self.nombre)+"\", por que ya existe.", self.fila, self.columna))

    def comprobandoProfundidadArreglo(self,cont,arreglo):
        if isinstance(arreglo,list):
            return self.comprobandoProfundidadArreglo(cont + 1,arreglo[0])
        else : return cont

    
