import tkinter as tk
import gramatica as gramatica
from Retorno.Mensajes import Mensajes
from Retorno.ReporteErroresHtml import ReporteErroresHtml
import sys
print(sys.getrecursionlimit())
sys.setrecursionlimit(5000)
print("Aumento recursionLimit: "+str(sys.getrecursionlimit()))

#from navarro.navarro import *

#texto del archivo "entrada.ply"
textInput = ""
#texto, contador de lineas digitadas
lineCounter = ""
#raiz del entorno graficoejbo1234
root = tk.Tk()
root.title("Proyecto_1  /  Compiladores_1  /  Erick _Bernal_201480017")
root.geometry("1320x700")


#funciones 


#-----------------------  MAIN -------------------------------------------
def main():
    #capturando texto de la caja de texto
    data = str(tk_text_input.get("1.0",tk.END+"-1c"))
    print("----------------------- INICIO -----------------------------------")
    #Caracteres especiales
    data = data.replace('\\\\','\\')
    data = data.replace('\\n','\n')
    data = data.replace('\\N','\n')
    data = data.replace('\\t','\t')
    data = data.replace('\\T','\t')
    data = data.replace('\\r','\r')
    data = data.replace('\\R','\r')
    data = data.replace('\\"','\"')
    data = data.replace('\\\'','\'')
    data = data.lower()

    #Enviamos a ejecutar el texto ingresado
    listaMensajes = gramatica.analizar(data)
    #imprimiendo mensajes de salida
    outPutMsj = ""
    outPutError = ""
    contMsj = 0
    contError = 0
    for msj in listaMensajes:
        if msj.tipo == 5:
            outPutMsj = outPutMsj + str(contMsj) + ") "+ msj.getMensaje() + "\n"
            contMsj = contMsj + 1
        else:
            outPutError = outPutError  + str(contError) + ") "+ msj.getMensaje() + "\n"
            contError = contError + 1
    clearTextOutPut() 
    tk_text_Output.insert("1.0", outPutMsj)
    tk_text_OutputError.insert("1.0", outPutError)
    contLineasEjecutar()
    ReporteErroresHtml(listaMensajes)
    print("******************** Fin Ejecucion *************************")

#-----------------------  imprimir conteo lineas sin carga masiva  -------------------
def contLineasEjecutar():
    cont = 0
    texto = ""
    while(cont != 100):
        texto = texto + str(cont) + "\n"
        cont = cont + 1
    tk_line_counter.insert("1.0", texto)

#-----------------------  Mensajes  -------------------
def cargaMensajes(ListaMensajes):
    clearTextOutPut()
    msj = ""
    for m in ListaMensajes:
        msj += m.getMensaje() + "\n"
        #print(m.getMensaje())
    tk_text_Output.insert("1.0", msj)
    
#-----------------------  REPORTES  -------------------
def reports():
    print("reportes")

#---------------------  GENERALES ---------------
def clearTextInput():
    tk_text_input.delete("1.0","end")
    tk_line_counter.delete("1.0","end")

def clearTextOutPut():
    tk_text_Output.delete("1.0","end")
    tk_text_OutputError.delete("1.0","end")

def readFile():
    nameFile = "entrada.ply"
    #limpiar entrada de texto
    tk_text_input.delete("1.0","end")
    #open file
    f = open(nameFile,'r',encoding='utf-8')
    textInput = f.read()
    f.close()
    #insertar texto en la caja de texto
    tk_text_input.insert("1.0", textInput)
    #asignando el contador de lineas
    contadorDeLineasAlCargarArchivo(nameFile)


def contadorDeLineasAlCargarArchivo(nameFile):
    #limpiamos
    countLines = 0
    lineCounter = "0"
    tk_line_counter.delete("1.0","end")
    #leemos archivo
    f2 = open(nameFile,'r',encoding='utf-8')
    for line in f2:
        if countLines != 0:
            lineCounter = lineCounter + "\n" +str(countLines)
        countLines = countLines + 1

    f2.close()
    #insertamos texto en caja de texto
    tk_line_counter.insert("1.0", lineCounter)

def guardar():
    data = str(tk_text_input.get("1.0",tk.END+"-1c"))
    data = data.lower()
    f = open('entrada.ply','w')
    f.write(data)
    f.close()




# -----------------------------------programacion del entorno grafico y funcionalidad ------------------------
#variable de posicion x y y de botones
x_btn = 125
y_btn = 320
#creacion de la caja de entrada de texto,centrado autmaticamente con .pack()
tk_text_input = tk.Text(root, height=20,width = 110)
tk_text_input.place(x=55,y=10)

#contador de lineas de la entrada de texto
tk_line_counter = tk.Text(root,height=20,width = 5)
tk_line_counter.place(x=10,y=10)

#boton para abrir el archivo, que ejecuta su propio metodo
btnOpenFile = tk.Button(root,text="Cargar Archivo",bg="#00ff9c",command = readFile)
btnOpenFile.place(x= x_btn+30,y=y_btn)

#boton limpiar caja de texto
btnClear = tk.Button(root,text="Limpiar Entrada",command = clearTextInput,bg="#FFA500")
btnClear.place(x= x_btn+150,y=y_btn)

#boton princial, tendra la ejecucion del analizador lexico, sintactico y semantico
btnExecute = tk.Button(root,text="Ejecutar",command=main,bg="#1E90FF")
btnExecute.place(x= x_btn+280,y=y_btn)

#boton reportes
btnReport = tk.Button(root,text="Reportes",command = reports,bg="#00f7ff")
btnReport.place(x= x_btn+ 365,y=y_btn)

#boton limpiar salida
btnClearOutPut = tk.Button(root,text="Limpiar Salida ",command = clearTextOutPut,bg="#FFA500")
btnClearOutPut.place(x= x_btn+125,y=y_btn + 285)

#Boton guardar texto
btnGuardar = tk.Button(root,text="Guardar",command=guardar,bg="#FFA500")
btnGuardar.place(x= x_btn+ 465,y=y_btn)

#caja de texto, de salida de informacion (operaciones del lenguaje "EPK")
tk_text_Output = tk.Text(root, height = 15, width = 70)
tk_text_Output.place(x = 10, y = y_btn +50)


#caja de text, salida de errores
tk_text_OutputError = tk.Text(root,height = 15 , width = 113)
tk_text_OutputError.place(x = 510, y = y_btn + 50 )

root.mainloop()