from Interface.Instruccion import Instruccion
from Entorno.Tipo import Tipo
from Entorno.Entorno import Entorno
from Entorno.Simbolo import Simbolo
from Retorno.Mensajes import Mensajes
from Instrucciones.ARREGLOS.ArregloInstancia import ArregloInstancia
from Instrucciones.ARREGLOS.ArregloLista import ArregloLista


class LLamadaArreglo(Instruccion):
    def __init__(self, fila, columna,nombreArr,Lexp):
        self.fila       = fila
        self.columna    = columna
        self.nombreArr  = nombreArr
        self.Lexp        = Lexp
        self.tipo       = Tipo.NULLO
        self.coordenada = []
        self.arreglo = []


    def ejecutar(self, ent:Entorno, listaMensajes):
        self.coordenada.clear()#como llamaremos el mismo objeto(llamadaArreglo) mas de una vez, reiniciamos las coordenadas para evitar, tener valor erroneos
        #  comprobar si existe el Arreglo
        sim:Simbolo = ent.buscarSimbolo(self.nombreArr)
        if sim is not None:
            claseArreglo = sim.valor# claseArreglo, puede ser arreglo Tipo1 o Tipo2 (instancia,lista)
            if isinstance(claseArreglo, ArregloLista) or isinstance(claseArreglo, ArregloInstancia):
                self.arreglo = claseArreglo.listaDedimensiones
            #Buscando la coordenadas, de la llamada, del  vector
            for exp in self.Lexp:
                retorno = exp.ejecutar(ent,listaMensajes)
                if exp.tipo == Tipo.ENTERO:
                    self.coordenada.append(retorno)
                else : 
                    listaMensajes.append(Mensajes(3, "LLamadaArreglo", "De tipos", "La Exp del arreglo:\""+str(self.nombreArr)+"\", no es de tipo INT es:("+str(exp.tipo)+")", self.fila, self.columna))
                    return Tipo.NULLO

            #comprobamos que el arreglo tenga la misma profundidas que en la llamada
            if claseArreglo.cantDim == len(self.Lexp):
                #Si, todo esta correcto, realizamos la llamada a la posicion del vector
                retorno = self.buscarCoordenada(0, self.coordenada, self.arreglo,listaMensajes)
                #comprobar que el retorno sea correcto,retorno != NULL (que la coordenada exista)
                # si retorna un Tipo.NULLO, significa que el arreglo unicamente fue inicializado, mas no se le han asignado valores 
                if retorno == Tipo.NULLO:
                    self.tipo = Tipo.NULLO
                    listaMensajes.append(Mensajes(3, "LLamadaArreglo", "De tipos", "se intenta acceder/retornar, una posicion del Arreglo: \""+str(self.nombreArr)+"\",  con valor = NULL, asigne un valor para poder operar esta posicion ", self.fila, self.columna))
                    return Tipo.NULLO
                elif retorno is not None:
                    #si todo esta correcto, 
                    # 1. asignamos el tipo, a la llamada (para que pueda operarse, en cualquier Expresion)
                    # 2. retornamos el valor del primitivo
                    self.tipo = claseArreglo.tipo
                    if isinstance(claseArreglo, ArregloLista):# el arreglo por lista T2, guarda diferente los datos (tipo primitivo,hay ejecutarlos para retornar el valor)
                       return retorno.ejecutar(ent,listaMensajes)
                    return retorno# si es ARREGlo T1, este solo guarda datos(valor).
                else :
                    return Tipo.NULLO                    
            else :
                listaMensajes.append(Mensajes(3, "LLamadaArreglo", "De dimensiones", "la profundidad/dimensiones del arreglo guardado: \""+str(self.nombreArr)+"\", no concuerda con el arreglo en llamada ", self.fila, self.columna))
                return Tipo.NULLO
        else : 
            listaMensajes.append(Mensajes(3, "LLamadaArreglo", "Declaracion", "el Arreglo: \""+str(self.nombreArr)+"\", no existe.", self.fila, self.columna))

        return Tipo.NULLO    


    def buscarCoordenada(self,cont,lcoordenadas,arreglo,listaMensajes):
        if isinstance(arreglo, list):
            pos = lcoordenadas[cont] #por que las coordenadas, las manejares desde 0 (cero), y la expresion nos retorna desde 1
            #comprobamos que el arreglo cuente con un tamaño que este dentro de la coordenada
            if pos < len(arreglo):
                return self.buscarCoordenada(cont + 1 , lcoordenadas, arreglo[pos],listaMensajes)  
            else : 
                #retorno de Error, por que se quiere acceder, a una posicion del arreglo, que no existe 
                listaMensajes.append(Mensajes(3, "LLamadaArreglo", "Dimensiones", "se busca una dimension que esta fuera de los limites del arreglo: \""+str(self.nombreArr)+"\" ", self.fila, self.columna))            
                return None
        else : 
            return arreglo # retornara el ultimo dato del arreglo (dato tipo primitivo,data buscado)

    def getTipo(self):
        return self.tipo