from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno
from Retorno.Mensajes import Mensajes
from Instrucciones.IF.ELSEIF import ELSEIF
from Instrucciones.IF.ELSE import ELSE
from Instrucciones.Break import Break
from Instrucciones.Return import Return



class IF(Instruccion):
    def __init__(self, fila, columna,condIf,instIf,l_ElseIf,Else:ELSE):
        self.fila = fila
        self.columna = columna
        self.condIf = condIf # condIf es una Exprecion (E)
        self.instIf = instIf
        self.l_ElseIf = l_ElseIf #listado de todos los posibles ElseIf
        self.Else = Else
    
    def ejecutar(self, entPadre:Entorno, listaMensajes):
        banderaElse = False
        entCond = Entorno(entPadre)# el entornoIf es necesario, para no guardar ninguna variable en el entorno padre.
        valor = self.condIf.ejecutar(entCond,listaMensajes) #debe retornoar un boolean,por ser una exp (E)
        

        if valor:# si la condicion es True, entramos al if , las instrucciones
            entInstrucciones = Entorno(entPadre)
            for ins in self.instIf:
                retorno = ins.ejecutar(entInstrucciones,listaMensajes) #se usa un entorno nuevo para no guardar nada en el padre 
                if isinstance(retorno, Break):
                    return retorno
                elif isinstance(retorno, Return):
                    return retorno
            banderaElse = True

        #lista de else if
        elif self.l_ElseIf != None: #
            for i in self.l_ElseIf:
                entElseIf = Entorno(entPadre) #entorno independiente para cada else if
                banderaElse = i.ejecutar(entElseIf,listaMensajes)
                if isinstance(banderaElse, bool):
                    if banderaElse    :  break
                if isinstance(banderaElse, Break):
                    return banderaElse
                elif isinstance(banderaElse, Return):
                    return banderaElse
            
        if self.Else != None and not banderaElse :
            entElse = Entorno(entPadre)
            retorno = self.Else.ejecutar(entElse, listaMensajes)
            if isinstance(retorno, Break):
                return retorno
            elif isinstance(retorno, Return):
                    return retorno





