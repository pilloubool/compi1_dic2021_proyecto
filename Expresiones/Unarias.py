from Interface.Instruccion import Instruccion
from Entorno.Tipo import Tipo
from Entorno.Entorno import Entorno
from Retorno.Mensajes import Mensajes
from Entorno.Simbolo import Simbolo



class Unarias(Instruccion):
    def __init__(self, fila, columna, operador, exprecion):
        self.fila = fila
        self.columna = columna
        self.operador = operador
        self.exprecion = exprecion
        self.tipo = Tipo.NULLO
    # ..............................................................................
    
    def ejecutar(self, ent : Entorno, ListaMensajes):
        valor = self.exprecion.ejecutar(ent,ListaMensajes)
        self.tipo = self.exprecion.tipo

        if self.operador == '-':
            if self.tipo == Tipo.ENTERO :   return int(valor) * -1
            elif self.tipo == Tipo.DOBLE:   return float(valor) * -1
            self.tipo = Tipo.NULLO
            ListaMensajes.append(Mensajes(3, "-num", "-(no Num)", "se intenta negar un valor que no es Num", self.fila, self.columna))
            return None

        elif self.operador == '!':
            if self.tipo == Tipo.BOOLEANO   :   return not bool(valor)
            ListaMensajes.append(Mensajes(3, "! exp", "!(exp)", "No es posible negar una exp, que no sea Booleana", self.fila, self.columna))
            self.tipo = Tipo.NULLO
            return None
            print("------  !   -------")

        ListaMensajes.append(Mensajes(6, "UNARIOS", "Unario con NoUnario","Se asigno un valor unario, a una exp de tipo: "+str(self.tipo) , self.fila, self.columna))
        return None
    # ..............................................................................

    def getTipo(self):
        return self.tipo
    # ..............................................................................
