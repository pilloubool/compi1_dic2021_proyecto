class Mensajes:

    def __init__(self,tipo,nombreInstruccion,error,descripcion,fila,columna):
        self.tipo = tipo
        self.nombreInstruccion = str(nombreInstruccion)
        self.descripcion = str(descripcion)
        self.fila = str(fila)
        self.columna = str(columna)
        self.error = str(error)

    def getMensaje(self):
        t = ""
        er = " , Error:<"+str(self.error)+ ">"
        err = "?"
        if self.tipo == 1 :   t = "(Error.Lexico)"
        elif self.tipo == 2 : t =  "(Error.Semantico)"
        elif self.tipo == 3 : t =  "(Error.Sintactico)"
        elif self.tipo == 4 : 
            t =  "Mensaje: "
            er = "( "+str(self.error)+" )"
            err = "*"
        elif self.tipo == 5 :  return  self.descripcion  #+ "         {lin:"+str(self.fila) +" , col:"+ str(self.columna)+"} "
        elif self.tipo == 6 :  return err + " (Error): "+ self.descripcion + "   (fila:"+str(self.fila) +" , col:"+ str(self.columna)+") "
        else : return   "(tipo Incorrecto)"

        
        
        msj = err +" <<< " + t + " , Inst:"+ str(self.nombreInstruccion)+ er +" , Descripcion:"+ self.descripcion +" , " + " (fila:"+str(self.fila) +" , col:"+ str(self.columna)+")  >>>"
        return str(msj)
        
    def getTipoError(self):
        if self.tipo == 1 :   return "(Error.Lexico)"
        elif self.tipo == 2 : return "(Error.Semantico)"
        elif self.tipo == 3 : return  "(Error.Sintactico)"
        elif self.tipo == 4 : return  "Mensaje: "
        elif self.tipo == 6 : return  "(Error): "
