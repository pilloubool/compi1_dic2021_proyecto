from Interface.Instruccion import Instruccion
from Entorno.Tipo import Tipo
from Retorno.Mensajes import Mensajes


class Logicas(Instruccion):
    def __init__(self, fila, columna, operador, hijoIzq, hijoDer):
        self.fila = fila
        self.columna = columna
        self.operador = operador
        self.hijoIzq = hijoIzq
        self.hijoDer = hijoDer
        self.tipo = Tipo.BOOLEANO

    def ejecutar(self, entorno, listaMensajes):
        izq = self.hijoIzq.ejecutar(entorno,listaMensajes)
        der = self.hijoDer.ejecutar(entorno,listaMensajes)
        if self.hijoIzq.tipo == Tipo.BOOLEANO and self.hijoDer.tipo == Tipo.BOOLEANO:
            if self.operador == '||':
                if izq or der : return True
                return False
            elif self.operador == '&&':
                if izq and der : return True
                return False

        listaMensajes.append(Mensajes(3, "Op. Logica : \""+str(self.operador)+"\"","No es logica", "No puede realizarse una operacion logica, si las exp != boolean", self.fila, self.columna))
        self.tipo = Tipo.NULLO
        return None
    # ..............................................................................

    def getTipo(self):
        return self.tipo