from Entorno.Simbolo import Simbolo


class Entorno:
    def __init__(self, anterior=None):
        self.anterior = anterior
        self.tablaSimbolos = {}

    def nuevoSimbolo(self, simbolo: Simbolo):
        s = self.tablaSimbolos.get(simbolo.nombre)

        if s == None:
            self.tablaSimbolos[simbolo.nombre] = simbolo
            return "ok"

        return "Ya existe un símbolo con esa llave"

    def editarSimbolo(self, llave, nuevo: Simbolo):
        #este metodo solo edita en el Estado actual, no busca la variable en entornos anteriores, deberia agregarse buscarSimbolo()
        s = self.tablaSimbolos.get(llave)

        if s != None:
            self.tablaSimbolos[llave] = nuevo
            return "Se ha editado el simbolo " + llave

        return "No existe símbolo con llave: " + llave

    def eliminarSimbolo(self, llave):
        s = self.tablaSimbolos.get(llave)

        if s != None:
            del self.tablaSimbolos[llave]
            return "ok"

        return "El símbolo a eliminar no existe"

    def buscarSimbolo(self, llave):
        ent = self

        while ent != None:
            s = ent.tablaSimbolos.get(llave)
            if s != None:
                return s
            ent = ent.anterior

        return None

    def getTablaSimbolos(self):
        return self.tablaSimbolos
