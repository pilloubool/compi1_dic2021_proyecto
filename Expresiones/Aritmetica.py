from Interface.Instruccion import Instruccion
from Expresiones.Identificador import Identificador
from Retorno.Mensajes import Mensajes
from Entorno.Tipo import Tipo

class Aritmetica(Instruccion):
    def __init__(self, fila, columna, operador, hijoIzq, hijoDer):
        self.fila = fila
        self.columna = columna
        self.operador = operador
        self.hijoIzq = hijoIzq
        self.hijoDer = hijoDer
        self.tipo = Tipo.NULLO

    def ejecutar(self, entorno,listaMensajes):
        izq = self.hijoIzq.ejecutar(entorno,listaMensajes)
        der = self.hijoDer.ejecutar(entorno,listaMensajes)

        if self.operador == '+':
            if self.hijoIzq.tipo == Tipo.ENTERO :   return self.sumar_Hizq_int(listaMensajes,izq,der)
            elif self.hijoIzq.tipo == Tipo.DOBLE:   return self.sumar_Hizq_doble(listaMensajes,izq,der)
            elif self.hijoIzq.tipo == Tipo.BOOLEANO:   return self.sumar_Hizq_Boolean(listaMensajes,izq,der)
            elif self.hijoIzq.tipo == Tipo.CHAR :   return self.sumar_Hizq_Char(listaMensajes, izq, der)
            elif self.hijoIzq.tipo == Tipo.CADENA:  return self.sumar_Hizq_String(listaMensajes, izq, der)
        elif self.operador == '-':
            if self.hijoIzq.tipo == Tipo.ENTERO         :   return self.restar_Hizq_int(listaMensajes,izq,der)
            elif self.hijoIzq.tipo == Tipo.DOBLE        :   return self.restar_Hizq_double(listaMensajes,izq,der)
            elif self.hijoIzq.tipo == Tipo.BOOLEANO     :   return self.restar_Hizq_booleano(listaMensajes,izq,der)
        elif self.operador == '*':
            if self.hijoIzq.tipo == Tipo.ENTERO     :   return self.multi_Hizq_int(listaMensajes,izq,der)
            elif self.hijoIzq.tipo == Tipo.DOBLE    :   return self.multi_Hizq_double(listaMensajes,izq,der)
        elif self.operador == '/':
            if self.hijoIzq.tipo == Tipo.ENTERO or self.hijoIzq.tipo == Tipo.DOBLE  :   return self.div_Hizq_intDoble(listaMensajes,izq,der)
        elif self.operador == '**':
            if self.hijoIzq.tipo == Tipo.ENTERO     :   return self.pow_Hizq_int(listaMensajes,izq,der)
            elif self.hijoIzq.tipo == Tipo.DOBLE    :   return self.pow_Hizq_double(listaMensajes,izq,der)
        elif self.operador == '%':
             if self.hijoIzq.tipo == Tipo.ENTERO or self.hijoIzq.tipo == Tipo.DOBLE : return self.modulo_Hizq_intDoble(listaMensajes,izq,der)

        self.tipo = Tipo.NULLO
        listaMensajes.append(Mensajes(6, "Aritmetica", "er","no esta definido esta exprecion aritmetica: " +str(self.operador), self.fila, self.columna))
        return None





    # ..............................................................................
    def castearObj(self, tipo, valor):
        if tipo == Tipo.ENTERO:
            return int(valor)
        elif tipo == Tipo.CADENA:
            return str(valor)
        elif tipo == Tipo.DOBLE:
            return float(valor)
        elif tipo == Tipo.BOOLEANO:
            return bool(valor)
        elif tipo == Tipo.CHAR:
            return chr(valor)
    # ..............................................................................
    def getTipo(self):
        return self.tipo

    # """"""""""""""""""""""""""""""""""""""""""""  metodos funcionales para realizar la Aritmetica """"""""""""""""""""""""""""""""""""""""""""""""""""

    # ........................................ SUMA ......................................

    def sumar_Hizq_int(self,listaMensajes,izq,der):
        # Hijo_izq = Tipo.ENTERO
        castIzq = self.castearObj(Tipo.ENTERO, izq)

        if self.hijoDer.tipo == Tipo.ENTERO:
            self.tipo = Tipo.ENTERO
            return castIzq + self.castearObj(Tipo.ENTERO, der)
        elif self.hijoDer.tipo == Tipo.DOBLE:
            self.tipo = Tipo.ENTERO
            return castIzq + self.castearObj(Tipo.ENTERO, der)
        elif self.hijoDer.tipo == Tipo.BOOLEANO:
            self.tipo = Tipo.ENTERO
            return castIzq + int(self.castearObj(Tipo.BOOLEANO, der))
        elif self.hijoDer.tipo == Tipo.CADENA:
            self.tipo = Tipo.CADENA
            return str(self.castearObj(Tipo.ENTERO, izq)) + self.castearObj(Tipo.CADENA, der)
        # sector de Error en suma
        elif self.hijoDer.tipo == Tipo.CHAR: # no se puede sumar un int con un char
            self.tipo = Tipo.NULLO
            listaMensajes.append(Mensajes(3, "Suma", "De tipos: (int + char)", "no es posible realizar esta suma", self.hijoDer.fila, self.hijoDer.columna))
            return None

        listaMensajes.append(Mensajes(6, "SUMA", "int y noPrimitivo", "no se puede sumar un int con un dato que no sea Primitivo", self.fila, self.columna))
        return None

    def sumar_Hizq_doble(self,listaMensajes,izq,der): #..............................................................................
        # Hijo_izq = Tipo.DOBLE
        castIzq = self.castearObj(Tipo.DOBLE, izq)

        if self.hijoDer.tipo == Tipo.ENTERO:
            self.tipo = Tipo.DOBLE
            return castIzq + float(self.castearObj(Tipo.ENTERO, der))
        elif self.hijoDer.tipo == Tipo.DOBLE:
            self.tipo = Tipo.DOBLE
            return castIzq + self.castearObj(Tipo.DOBLE, der)
        elif self.hijoDer.tipo == Tipo.BOOLEANO:
            self.tipo = Tipo.DOBLE
            return castIzq + float(int(self.castearObj(Tipo.BOOLEANO, der)))
        elif self.hijoDer.tipo == Tipo.CHAR:
            self.tipo = Tipo.NULLO
            listaMensajes.append(Mensajes(3, "Suma", "De tipos: (Double + char)", "no es posible realizar esta suma", self.hijoDer.fila, self.hijoDer.columna))
            return None
        elif self.hijoDer.tipo == Tipo.CADENA:
            self.tipo = Tipo.CADENA
            return str(castIzq) + self.castearObj(Tipo.CADENA, der)

        listaMensajes.append(Mensajes(6, "SUMA", "Double y noPrimitivo", "no se puede sumar un Double, con un dato que no sea Primitivo", self.fila, self.columna))
        return None

    def sumar_Hizq_Boolean(self,listaMensajes,izq,der): #..............................................................................
        castIzq = self.castearObj(Tipo.BOOLEANO, izq)

        if self.hijoDer.tipo == Tipo.ENTERO:
            self.tipo = Tipo.ENTERO
            return int(castIzq) + self.castearObj(Tipo.ENTERO, der)
        elif self.hijoDer.tipo == Tipo.DOBLE:
            self.tipo = Tipo.DOBLE
            return float(int(castIzq)) + self.castearObj(Tipo.DOBLE, der)
        elif self.hijoDer.tipo == Tipo.BOOLEANO:
            self.tipo = Tipo.ENTERO
            return int(castIzq) + int(self.castearObj(Tipo.BOOLEANO, der))
        elif self.hijoDer.tipo == Tipo.CHAR:
            self.tipo = Tipo.NULLO
            listaMensajes.append(Mensajes(3, "Suma", "De tipos: (Boolean + char)", "no es posible realizar esta suma", self.hijoDer.fila, self.hijoDer.columna))
            return None
        elif self.hijoDer.tipo == Tipo.CADENA:
            self.tipo = Tipo.CADENA
            return str(castIzq) + self.castearObj(Tipo.CADENA, der)
        
        listaMensajes.append(Mensajes(6, "SUMA", "Boolean y noPrimitivo", "no se puede sumar un Boolean, con un dato que no sea Primitivo", self.fila, self.columna))
        return None

    def sumar_Hizq_Char(self,listaMensajes,izq,der): #..............................................................................
        castIzq = self.castearObj(Tipo.CADENA, izq)

        if self.hijoDer.tipo == Tipo.CHAR  or self.hijoDer.tipo == Tipo.CADENA:
            self.tipo = Tipo.CADENA
            return castIzq + str(der)
        else:
            self.tipo = Tipo.NULLO
            listaMensajes.append(Mensajes(3, "Suma", "De tipos: (CHAR + (int,double,boolena))", "no es posible realizar esta suma", self.hijoDer.fila, self.hijoDer.columna))
            return None
        
        listaMensajes.append(Mensajes(6, "SUMA", "CHAR y noPrimitivo", "no se puede sumar un CHAR, con un dato que no sea Primitivo", self.fila, self.columna))
        return None
 
    def sumar_Hizq_String(self,listaMensajes,izq,der): #..............................................................................
        castIzq = self.castearObj(Tipo.CADENA, izq)

        if self.hijoDer.tipo == Tipo.ENTERO or self.hijoDer.tipo == Tipo.DOBLE or self.hijoDer.tipo == Tipo.CADENA or self.hijoDer.tipo == Tipo.BOOLEANO  or self.hijoDer.tipo == Tipo.CHAR:
            self.tipo = Tipo.CADENA
            return castIzq + str(der)

        listaMensajes.append(Mensajes(6, "SUMA", "STRING y noPrimitivo", "no se puede sumar un STRING, con un dato que no sea Primitivo", self.fila, self.columna))
        return None

    # ........................................ RESTA ......................................
    def restar_Hizq_int(self,listaMensajes,izq,der):
        castIzq = self.castearObj(Tipo.ENTERO, izq)

        if self.hijoDer.tipo == Tipo.ENTERO:
            self.tipo = Tipo.ENTERO
            return castIzq - self.castearObj(Tipo.ENTERO, der)
        elif self.hijoDer.tipo == Tipo.DOBLE:
            self.tipo = Tipo.DOBLE
            return float(castIzq) - self.castearObj(Tipo.DOBLE, der)
        elif self.hijoDer.tipo == Tipo.BOOLEANO or self.hijoDer.tipo == Tipo.CHAR or self.hijoDer.tipo == Tipo.CADENA:
            self.tipo = Tipo.NULLO
            listaMensajes.append(Mensajes(3, "RESTA", "De tipos: (INT - (string,char,boolena))", "no es posible realizar esta Resta", self.hijoDer.fila, self.hijoDer.columna))
            return None
        
        listaMensajes.append(Mensajes(6, "RESTA", "ENTERO y noPrimitivo", "no se puede sumar un STRING, con un dato que no sea Primitivo", self.fila, self.columna))
        return None

    def restar_Hizq_double(self,listaMensajes,izq,der): #..............................................................................
        castIzq = self.castearObj(Tipo.DOBLE, izq)
        self.tipo = Tipo.DOBLE

        if self.hijoDer.tipo == Tipo.ENTERO or self.hijoDer.tipo == Tipo.DOBLE:
            return castIzq - self.castearObj(Tipo.DOBLE, der)
        elif self.hijoDer.tipo == Tipo.BOOLEANO:
            return castIzq - float(int(self.castearObj(Tipo.BOOLEANO, der)))
        elif self.hijoDer.tipo == Tipo.CHAR or self.hijoDer.tipo == Tipo.CADENA:
            self.tipo = Tipo.NULLO
            listaMensajes.append(Mensajes(3, "RESTA", "De tipos: (Double - (string,char))", "no es posible realizar esta Resta", self.hijoDer.fila, self.hijoDer.columna))
            return None

        listaMensajes.append(Mensajes(6, "RESTA", "DOUBLE y noPrimitivo", "no se puede sumar un DOUBLE, con un dato que no sea Primitivo", self.fila, self.columna))
        return None   
            
    def restar_Hizq_booleano(self,listaMensajes,izq,der):#..............................................................................
        castIzq = self.castearObj(Tipo.BOOLEANO,izq)

        if self.hijoDer.tipo == Tipo.ENTERO:
            self.tipo = Tipo.ENTERO
            return int(castIzq) - self.castearObj(Tipo.ENTERO, der)
        elif self.hijoDer.tipo == Tipo.DOBLE:
            self.tipo = Tipo.DOBLE
            return float(int(castIzq)) - self.castearObj(Tipo.DOBLE, der)
        elif self.hijoDer.tipo == Tipo.BOOLEANO or self.hijoDer.tipo == Tipo.CHAR or self.hijoDer.tipo == Tipo.CADENA:
            self.tipo = Tipo.NULLO
            listaMensajes.append(Mensajes(3, "RESTA", "De tipos: (Boolean - (boolean,string,char))", "no es posible realizar Resta", self.hijoDer.fila, self.hijoDer.columna))
            return None

        listaMensajes.append(Mensajes(6, "RESTA", "BOOLEAN y noPrimitivo", "no se puede sumar un BOOLEAN, con un dato que no sea Primitivo", self.fila, self.columna))
        return None  

    # ........................................ MULTIPLICACION ......................................

    def multi_Hizq_int(self,listaMensajes,izq,der):
        castIzq = self.castearObj(Tipo.ENTERO, izq)

        if self.hijoDer.tipo == Tipo.ENTERO:
            self.tipo = Tipo.ENTERO
            return castIzq * self.castearObj(Tipo.ENTERO, der)
        elif self.hijoDer.tipo == Tipo.DOBLE:
            self.tipo = Tipo.DOBLE
            return float(castIzq) * self.castearObj(Tipo.DOBLE, der)
        
        self.tipo = NoTipo.NULLOne
        listaMensajes.append(Mensajes(3, "MULT", "De tipos: (MULT * (boolean,string,char))", "no es posible realizar  MULT", self.hijoDer.fila, self.hijoDer.columna))
        return None

    def multi_Hizq_double(self,listaMensajes,izq,der):

        if self.hijoDer.tipo == Tipo.ENTERO or self.hijoDer.tipo == Tipo.DOBLE:
            self.tipo = Tipo.DOBLE
            return self.castearObj(Tipo.DOBLE, izq) * self.castearObj(Tipo.DOBLE, der)

        self.tipo = Tipo.NULLO
        listaMensajes.append(Mensajes(3, "MULT", "De tipos: (MULT * (boolean,string,char))", "no es posible realizar  MULT", self.hijoDer.fila, self.hijoDer.columna))
        return None

    # ........................................ DIVICION ......................................

    def div_Hizq_intDoble(self,listaMensajes,izq,der):

        if self.hijoDer.tipo == Tipo.ENTERO or self.hijoDer.tipo == Tipo.DOBLE:
            self.tipo = Tipo.DOBLE
            return float(izq) / float(der)
        
        self.tipo = Tipo.NULLO
        listaMensajes.append(Mensajes(3, "DIV", "De tipos: (DIV * (boolean,string,char))", "no es posible realizar  DIV", self.hijoDer.fila, self.hijoDer.columna))
        return None


    # ........................................ DIVICION ......................................

    def pow_Hizq_int(self,listaMensajes,izq,der):
        if self.hijoDer.tipo == Tipo.ENTERO:
            self.tipo = Tipo.ENTERO
            return pow(int(izq), int(der))
        elif self.hijoDer.tipo == Tipo.DOBLE:
            self.tipo = Tipo.DOBLE
            return pow(float(izq), float(der))

        self.tipo = Tipo.NULLO
        listaMensajes.append(Mensajes(3, "POW", "De tipos: (POW * (boolean,string,char))", "no es posible realizar  POW", self.hijoDer.fila, self.hijoDer.columna))
        return None

    def pow_Hizq_double(self,listaMensajes,izq,der):
        if self.hijoDer.tipo == Tipo.ENTERO or self.hijoDer.tipo == Tipo.DOBLE:
            self.tipo = Tipo.DOBLE
            return pow(float(izq), float(der))
            
        self.tipo = Tipo.NULLO
        listaMensajes.append(Mensajes(3, "POW", "De tipos: (POW * (boolean,string,char))", "no es posible realizar  POW", self.hijoDer.fila, self.hijoDer.columna))
        return None

    # ........................................ MODULO ......................................
    def modulo_Hizq_intDoble(self,listaMensajes,izq,der):
        if self.hijoDer.tipo == Tipo.ENTERO or self.hijoDer.tipo == Tipo.DOBLE:
            self.tipo = Tipo.DOBLE
            return float(izq) % float(der)
        self.tipo = Tipo.NULLO
        listaMensajes.append(Mensajes(3, "MODULO", "De tipos: (MODULO * (boolean,string,char))", "no es posible realizar  MODULO", self.hijoDer.fila, self.hijoDer.columna))
        return None








































