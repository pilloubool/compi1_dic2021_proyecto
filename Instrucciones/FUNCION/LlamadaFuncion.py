from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno
from Entorno.Simbolo import Simbolo
from Retorno.Mensajes import Mensajes
from Entorno.Tipo import Tipo
from Instrucciones.FUNCION.Funcion import Funcion
from Instrucciones.Return import Return
from Instrucciones.ARREGLOS.ArregloInstancia import ArregloInstancia

class LlamadaFuncion(Instruccion):
    def __init__(self, fila, columna,nombreFunc,lparam):
        self.fila = fila
        self.columna = columna
        self.nombreFunc = nombreFunc
        self.lparam = lparam
        self.tipo = Tipo.NULLO

    def ejecutar(self, entPadre:Entorno, listaMensajes):
        entLLamada = Entorno(entPadre)

        '''
        Nota: 
        *   la clase Funcion, se encarga de guardar una funcion en el entorno, si y solo si, ESTA BIEN DECLARADA.
        *   El paso 5, se ejecuta de manera independiente, no afecta a la salia original, solo sirve para validaciones de tipos, por ello usa los  AUXILIARES (AUX)
        
        pasos
        1. buscamos en el Entorno(tab sim) y retornamos la funcion para realizar las validaciones
            1.1 comprobamos que los nombres tanto de llamada como de la funcion guardada sean los mismo
        2. comprobamos # de parametros en llamada y en funcion (deben ser iguales)
        3. comprobamos que los tipos de los parametros sean los mismos 

        4. guardar los parametros de la funcion,con los valores de la (lparam) de la llamada, en un entorno auxiliar(entLLamada), para ejecutar la llamada
        5. comprobamos que el tipo de Return sea igual al tipo de la funcion
            5.1 ejecutamos todas las instrucciones en un entorno auxiliar para comprobar que los tipos de la Funcion y el Return sean iguales 

           <<Si Todo esta correcto,ejecutamos>>
        6. ejecutamos la lista de instrucciones, que estan dentro de la funcion, por que no hay ningun error
       
        '''
        #1)
        sim:Simbolo = entLLamada.buscarSimbolo(self.nombreFunc)
        if sim is not None:
            #2)
            func:Funcion = sim.valor
            self.tipo = func.tipo
            if len(func.Lparametros)==len(self.lparam):
                #3)
                cont = 0
                for par in self.lparam:
                    retorno = par.ejecutar(entLLamada,listaMensajes)
                    t2 = func.Lparametros[cont] # es de la clase Parametro 
                    if par.tipo != t2.tipo:
                        listaMensajes.append(Mensajes(3, "LLamadaFuncion", "De tipos", "Funcion: \""+str(self.nombreFunc)+"\", el parametro esperado es de tipo: \""+str(t2.tipo)+"\", pero se envio:\""+str(par.tipo)+"\"", self.fila, self.columna))
                        return Tipo.NULLO
                    cont = cont + 1
                #4)-----------------------------------------------  4  -----------------------------------------------
                cont = 0
               # entLLamada = Entorno(entPadre)
                for parmFuncion in func.Lparametros:
                    valorParm = self.lparam[cont].ejecutar(entLLamada,listaMensajes)# se ejecuta por que es una EXp(E)
                    if isinstance(parmFuncion,ArregloInstancia):
                        #si el parametro es de tipo arreglo 
                        entLLamada.nuevoSimbolo(Simbolo(parmFuncion.tipo,  parmFuncion.nombreArreglo, valorParm,  self.fila))
                    else:
                        #si el parametro es de tipo nativo
                        entLLamada.nuevoSimbolo(Simbolo(parmFuncion.tipo,  parmFuncion.id, valorParm,  self.fila))
                    cont = cont + 1
                '''
                #5)---------------------------------------------   5     -----------------------------------------------
                #LOS AUX sirven para comprobar tipos de la funcion y el retorno
                entAux = Entorno(entLLamada)
                listaAux = []# se usa una listaAux por que no queremos que se vea reflejado la ejecucion de todas las instrucciones, solo necesitamos comprobar los tipos de funcion y return
                    #5.1)
                for inst in func.Linstr:
                    inst.ejecutar(entAux,listaAux)#ejecucion de cada instruccion
                    if isinstance(inst, Return):
                        inst.exp.ejecutar(entAux,listaAux)#ejecucion de la exp del return
                        if inst.exp ==  None:
                            listaMensajes.append(Mensajes(3, "LLamadaFuncion", "De tipos", " la funcion: \""+str(func.nombre)+" ("+str(func.tipo)+")\" , no puede RETORNAR un valor de tipo:\"NULL\"", self.fila, self.columna))
                            return Tipo.NULLO
                        #si la expresion, en el Return, existe
                        if func.tipo == inst.exp.tipo:#comprobamos tipos
                            print("")
                            break
                        listaMensajes.append(Mensajes(3, "LLamadaFuncion", "De tipos", "la funcion: \""+str(func.nombre)+" ("+str(func.tipo)+")\" , no puede RETORNAR un valor de tipo:\""+str(inst.exp.tipo)+"\"", self.fila, self.columna))
                        return Tipo.NULLO
                '''
                #6)---------------------------------------- 6 ------------------------------------------
                self.tipo = func.tipo
                for inst in func.Linstr:
                    retorno = inst.ejecutar(entLLamada,listaMensajes)
                    if isinstance(retorno, Return):
                        #ejecutamos la exp
                        entAux = Entorno(entLLamada)
                        expResuelta = retorno.exp.ejecutar(entAux,listaMensajes)
                        return expResuelta

            else :
                listaMensajes.append(Mensajes(3, "LLamadaFuncion", "de Parametros", "no concuerda la cantidad de Parametros, de la LLadadaFuncion: \""+str(self.nombreFunc)+"\"", self.fila, self.columna))  
        else : 
            listaMensajes.append(Mensajes(3, "LLamadaFuncion", "No Existe funcion", "La funcion: \""+str(self.nombreFunc)+"\" no existe.", self.fila, self.columna))




    def getTipo(self):
        return self.tipo