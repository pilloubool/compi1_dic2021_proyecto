from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno
from Instrucciones.Break import Break
from Instrucciones.Return import Return



class ELSE(Instruccion):
    def __init__(self, fila, columna,instrucciones):
        self.fila  = fila
        self.columna = columna
        self.instrucciones = instrucciones

    def ejecutar(self, entElse:Entorno, listaMensajes):
        for i in self.instrucciones:
            retorno = i.ejecutar(entElse,listaMensajes)
            if isinstance(retorno, Break):  
                return retorno
            elif isinstance(retorno,Return):
                return retorno