class Error:
    def __init__(self, tipo, descripcion, fila, columna):
        self.tipo = tipo
        self.descripcion = descripcion
        self.fila = fila
        self.columna = columna

    def toString(self):
        return "Error " + self.tipo + ": " + self.descripcion + ". En fila " + str(self.fila) + " y columna " + str(self.columna)
