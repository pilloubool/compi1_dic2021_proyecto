from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno
from Instrucciones.Break import Break
from Instrucciones.Return import Return


class DoWhile(Instruccion):
    def __init__(self, fila, columna,instrucciones,condicion):
        self.fila = fila
        self.columna = columna
        self.instrucciones = instrucciones
        self.condicion = condicion

    def ejecutar(self, entPadre:Entorno, listaMensajes):
        primerCiclo = True
        cond = False
        #condicion debe ser boolean
        while(True):
            entDoWhile = Entorno(entPadre)
            if not primerCiclo:
                cond = self.condicion.ejecutar(entDoWhile,listaMensajes) #cond debera ser boolean para funcionar
                primerCiclo = True
            else : primerCiclo = False

            if isinstance(cond, bool):
                if cond or not primerCiclo:
                    for ins in self.instrucciones:
                        retorno =   ins.ejecutar(entDoWhile,listaMensajes)
                        if isinstance(retorno, Break):
                            return self # al aparecer break, retornaremos el propio doWhile, para salir unicamente de este ciclo y no de otros
                        elif isinstance(retorno, Return):
                            return retorno
                else : break
            else:
                listaMensajes.append(Mensajes(3, " Do WHILE ", "Exp no booleana", "condicion solo acepta Exp.Booleanas (permitidas)", self.fila, self.columna))
                break
