from Interface.Instruccion import Instruccion

class Break(Instruccion):
    def __init__(self, fila, columna):
        self.fila = fila
        self.columna = columna

    def ejecutar(self, entorno, listaMensajes):
        return self