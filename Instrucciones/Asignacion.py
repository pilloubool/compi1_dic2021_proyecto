from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno
from Entorno.Simbolo import Simbolo
from Retorno.Mensajes import Mensajes


class Asignacion(Instruccion):
    def __init__(self, fila, columna, identificador, expresion):
        self.fila = fila
        self.columna = columna
        self.identificador = identificador
        self.expresion = expresion

    def ejecutar(self, entorno: Entorno,listaMensajes):

        #Se ejecuta la exp a asignar, a la variable X.
        valor = self.expresion.ejecutar(entorno,listaMensajes)

        # validan que valor no retorne error, nulo o algo parecido
        if valor != None:
            varAmodificarValor: Simbolo = entorno.buscarSimbolo(self.identificador) #nos retorna el simbolo a modificar
            if varAmodificarValor != None:#comprobamos que el simbolo/variable exista
                if self.expresion.tipo == varAmodificarValor.tipo: # verificar errores semanticos, comprobacion de tipos
                    varAmodificarValor.valor = valor #Asignamos el nuevo valor
                    ''' 
                    #simbolo = entorno.editarSimbolo(self.identificador, Simbolo(varAmodificarValor.tipo, self.identificador,valor, self.fila))

                    el metodo de la clase entorno, solo funciona cuando la variable se encuentra declarada en el mismo entorno,
                    si la variable esta en un ENTORNO.ANTERIOR, no la encontrara.
                    por ello se plantea la solucion de unicamente cambiarle el valor al simbolo. usando el metodo
                    entorno.BUSCARSIMBOLO(), dado que este, busca en el ent actual y los ent anteriores, si lo encuentra 
                    retorna el SIMBOLO, por ello podemos modificar unicamente el atributo "valor"
                    '''
                else:
                    listaMensajes.append(Mensajes(3, "Asignacion", "De tipos", "La varAmodificarValor: \""+str(self.identificador)+"\" de tipo : "+str(varAmodificarValor.tipo)+", no puede recibir: "+str(self.expresion.tipo) , self.fila, self.columna))
            else:
                listaMensajes.append(Mensajes(3, "Asignacion", "Var no Existe", "La varAmodificarValor: \""+str(self.identificador)+"\" no ha sido declarada ", self.fila, self.columna))
        else:
            listaMensajes.append(Mensajes(3, "Asignacion", "Exp Erronea", "El valor a asignar es erroneo/no admitido, en var \""+str(self.identificador)+"\"", self.fila, self.columna))
